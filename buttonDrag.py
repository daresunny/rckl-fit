"""
UI element that implements the drag function: for order.
"""
import wx


class _Appearance:
    ChannelOrderBackGroundColor = wx.Colour(255, 255, 255)
    ChannelOrderForeGroundColor = wx.Colour(0, 0, 0)
    ChannelOrderDimensions = wx.Size(400, 25)
    tolerance = 20


class _Channel_Order_Button(wx.Button):
    def __init__(self, parent: wx.Panel, title: str):
        wx.Button.__init__(self, parent=parent, label=title)
        self.SetBackgroundColour(_Appearance.ChannelOrderBackGroundColor)
        self.SetForegroundColour(_Appearance.ChannelOrderForeGroundColor)

    def getx(self) -> int:
        """Gets the horizontal coordinates of the channel order button position."""
        x, _ = self.GetPosition().Get()
        return x


class OrderWidget (wx.Panel):
    """Widget useful to give order by drag and dropping."""
    def __init__(self, parent: wx.Window):
        super().__init__(parent, size=_Appearance.ChannelOrderDimensions)
        self._sizer = wx.BoxSizer(wx.HORIZONTAL)
        self._buttonPermutationVariables = self._ButtonPermutationVariables()
        self.SetSizer(self._sizer)
        self._numberOfElements = 0
        self._order: dict[_Channel_Order_Button, int] = {}

    class _ButtonPermutationVariables:
        def __init__(self):
            self.initialChannelOrderCoordinates: list[int] = []
            self.dragging: wx.Button = None
            self.translationFactor: int = 0
            self.channelOrderArray: list[_Channel_Order_Button] = []
            self.borders: tuple[int, int, int, int]
            self.currentIndex: int

        def getIndex(self, index: int) -> int:
            """Makes certain that input index is not out of bounds."""
            if index < 0:
                return 0
            if index > len(self.channelOrderArray) - 1:
                return len(self.channelOrderArray) - 1
            return index

    def createNewUnit(self, name: str) -> None:
        """Creates new button in the channel order."""
        newButton = _Channel_Order_Button(self, name)
        newButton.Bind(wx.EVT_MOTION, self._OnMouseMove)
        newButton.Bind(wx.EVT_LEFT_UP, self._OnMouseUp)
        newButton.Bind(wx.EVT_LEFT_DOWN, self._OnMouseDown)
        self._buttonPermutationVariables.channelOrderArray.append(newButton)
        self._sizer.Add(newButton, proportion=1)
        self._order[newButton] = self._numberOfElements
        self._numberOfElements += 1

    def getOrder(self) -> tuple[int]:
        """Gets the order based on the position of buttons."""
        L = []
        for button in self._buttonPermutationVariables.channelOrderArray:
            L.append(self._order[button])
        return tuple(L)

    def _OnMouseDown(self, event: wx.Event):
        if self._buttonPermutationVariables.dragging is not None:
            return
        selection = event.GetEventObject()
        buttons = self._buttonPermutationVariables.channelOrderArray
        coordinates = self._buttonPermutationVariables.initialChannelOrderCoordinates
        coordinates.clear()
        for button in buttons:
            x, _ = button.GetPosition().Get()
            coordinates.append(x)
        self._buttonPermutationVariables.dragging = selection
        sx, _ = selection.GetPosition().Get()
        dx, _ = self.ScreenToClient(wx.GetMousePosition().Get())
        xsize, ysize = self.GetSize().Get()
        tolerance = _Appearance.tolerance
        self._buttonPermutationVariables.borders = (- tolerance, xsize + tolerance,
                                                    - tolerance, ysize + tolerance)
        self._buttonPermutationVariables.translationFactor = sx - dx
        self._buttonPermutationVariables.currentIndex =\
            self._buttonPermutationVariables.channelOrderArray.index(selection)

    def _checkIfOut(self, x: int, y: int) -> bool:
        xmin, xmax, ymin, ymax = self._buttonPermutationVariables.borders
        return x < xmin or x > xmax or y < ymin or y > ymax

    def _OnMouseMove(self, _event: wx.Event):
        if self._buttonPermutationVariables.dragging is not None:
            selection = self._buttonPermutationVariables.dragging
            x, y = self.ScreenToClient(wx.GetMousePosition()).Get()
            _, cur_y = selection.GetPosition().Get()
            if self._checkIfOut(x, y):
                self._restartLocalPosition()
                return
            transl = self._buttonPermutationVariables.translationFactor
            selection.SetPosition(wx.Point(x + transl, cur_y))
            index = self._buttonPermutationVariables.currentIndex
            previndex = self._buttonPermutationVariables.getIndex(index - 1)
            nextindex = self._buttonPermutationVariables.getIndex(index + 1)
            prevx = self._buttonPermutationVariables.initialChannelOrderCoordinates[previndex]
            nextx = self._buttonPermutationVariables.initialChannelOrderCoordinates[nextindex]
            if x + transl < prevx:
                button = self._buttonPermutationVariables.channelOrderArray[previndex]
                self._buttonPermutationVariables.channelOrderArray[previndex] = selection
                self._buttonPermutationVariables.channelOrderArray[index] = button
                button.SetPosition(wx.Point(
                    self._buttonPermutationVariables.initialChannelOrderCoordinates[index], cur_y))
                self._buttonPermutationVariables.currentIndex = previndex
            if x + transl > nextx:
                button = self._buttonPermutationVariables.channelOrderArray[nextindex]
                self._buttonPermutationVariables.channelOrderArray[nextindex] = selection
                self._buttonPermutationVariables.channelOrderArray[index] = button
                button.SetPosition(wx.Point(
                    self._buttonPermutationVariables.initialChannelOrderCoordinates[index], cur_y))
                self._buttonPermutationVariables.currentIndex = nextindex

    def _OnMouseUp(self, _event: wx.Event):
        if self._buttonPermutationVariables.dragging is None:
            return
        self._restartLocalPosition()

    def _OnLeave(self, _event: wx.Event):
        self._restartLocalPosition()

    def _restartLocalPosition(self):
        self._sizer.Clear()
        for button in self._buttonPermutationVariables.channelOrderArray:
            self._sizer.Add(button, proportion=1)
        self._buttonPermutationVariables.dragging = None
        self.Layout()
