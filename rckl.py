#!/usr/bin/env python3
"""Calculate the appropriate RCKL values for the given data."""
from typing import Final
from typing import Callable
from random import randint
from random import seed
from bisect import insort
from math import ceil
from enum import IntEnum
from enum import unique
from abc import abstractmethod
import warnings
import sys
import threading
import time
import scipy.io
import numpy as np
from numpy.typing import NDArray
import matplotlib.pyplot as plt
from matplotlib.figure import Figure
from matplotlib.backend_bases import MouseButton
from matplotlib.backends.backend_pdf import PdfPages
from scipy.interpolate import CubicSpline
from scipy.optimize import minimize


from config import Config
from config import Alignment
from config import InterpolationType


@unique
class FittingVariables(IntEnum):
    """Enumeration of the parameters to draw."""
    R = 0
    C = 1
    K = 2
    L = 3


class Notifier:
    """
    Class for implementing a progress notifier: particularly in a GUI
    """
    def __init__(self):
        pass

    @abstractmethod
    def notifyUpdate(self, ticks, maxTicks) -> None:
        """
        Notifies of the current progress.
        """


class _CountDownLatch:
    def __init__(self, count: int = 0):
        self._maxcount: Final[int] = count
        self._count: int = count
        self._lock: Final[threading.Condition] = threading.Condition()

    def reset(self):
        """Resets the countdown to the maximal value"""
        self._count = self._maxcount

    def decrement(self) -> None:
        """Decrements the countdown."""
        self._lock.acquire()
        self._count -= 1
        if self._count <= 0:
            self._count = 0
            self._lock.notify_all()
        self._lock.release()

    def wait(self) -> None:
        """Waits until the countdown is zero."""
        self._lock.acquire()
        while self._count > 0:
            self._lock.wait()
        self._lock.release()


class ProgressUpdate:
    """Class for tracking progress of long executing tasks."""
    class _WrongCallOrderException(Exception):
        def __init__(self):
            super().__init__("Attempted to start progress," +
                             " before preparing the Updater.")

    def __init__(self, listeners: int = 0, waitTime: int = 1):
        self._condition: Final[threading.Condition] = threading.Condition()
        self._maxTicks: int
        self._ticks: int
        self._countdown = _CountDownLatch(listeners)
        self._waitTime = waitTime
        self._running = False
        self._started = False
        self._interrupt = False

    def setNewProgress(self, maxTicks: int) -> None:
        """Sets progress to track for a long executing task."""
        if self._started:
            raise self._WrongCallOrderException()
        self._countdown.reset()
        self._condition.acquire()
        self._maxTicks = maxTicks
        self._ticks = 0
        self._running = True
        self._started = True
        self._condition.notify_all()
        self._condition.release()

    def tick(self) -> bool:
        """Advances the progress."""
        if not self._started:
            raise self._WrongCallOrderException()
        self._ticks += 1
        if self._ticks >= self._maxTicks:
            self._ticks = self._maxTicks
            self._running = False
            self._countdown.wait()
            self._started = False

    def requestStop(self):
        """Requests the thread that is using the updater to stop."""
        self._condition.acquire()
        self._interrupt = True
        self._condition.release()

    def stopRequested(self) -> bool:
        self._condition.acquire()
        request = self._interrupt
        if request:
            self._running = False
            self._started = True
            self._condition.notify_all()
            self._condition.release()
            self._countdown.wait()
        else:
            self._condition.release()
        return request

    def subscribe(self, notifier: Notifier) -> None:
        """Subscribe to watch the progress increase and notify of updates."""
        self._condition.acquire()
        if not self._started:
            self._condition.wait()
        self._condition.release()
        while self._running:
            notifier.notifyUpdate(self._ticks, self._maxTicks)
            time.sleep(self._waitTime)
        self._countdown.decrement()


class DataLoadingException(Exception):
    """
    Exception that occurs if the .mat data cannot be loaded, or has incorrect format.
    """
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class _IOError(DataLoadingException):
    def __init__(self, filename: str):
        super().__init__("Unable to open the file " + filename + " . Check that it exists and is accessible")


class _WrongFormatException(DataLoadingException):
    def __init__(self, filename: str):
        super().__init__("The data in the file " + filename + " is formatted incorrectly.\n" +
                         "Please use the format as described in the README.")


class _WrongLengthException(DataLoadingException):
    def __init__(self, filename: str):
        super().__init__("The data in the file " + filename + " has columns of different lengths.\n" +
                         "Please make sure that all the three channels have the same length.")


class VisualBackend:
    """
    Class that manages plotting on the screen.
    """

    names: Final[list[str]] = ["R", "C", "K", "L"]
    units: Final[list[str]] = ["mmHg/(ml/min)", "ml/mmHg", "mmHg/(ml/min)", "mmHg/(ml/min²)"]

    def __init__(self):
        self._fontSize: int
        self._boxProp: dict
        self.dimension: int = 6

    def initializeProperties(self, fontSize, boxProp):
        """Initialize the appearance of the plots."""
        self._fontSize = fontSize
        self._boxProp = boxProp

    @classmethod
    def initializePeaksPlot(cls, ECG: float, peaks: []):
        """Initializes the Plot for finding peaks in the Electrocardiogram."""
        fig, ax = plt.subplots()
        points = []
        for peak in peaks:
            points.append(ax.plot(peak, ECG[peak], 'o', picker=True, color='r'))
        ECGPlotted = ax.plot(ECG, picker=True, color='b')
        plt.title('Click to add/remove peaks then press enter.')
        pointClicked = False
        curveClicked = False

        def on_pick(event):
            nonlocal points
            nonlocal pointClicked
            nonlocal curveClicked
            if event.artist == ECGPlotted[0]:
                curveClicked = True
                return
            pointClicked = True
            x = event.artist.get_data()[0][0]
            peaks.remove(x)
            event.artist.remove()
            plt.draw()

        def on_click(event):
            nonlocal points
            nonlocal pointClicked
            nonlocal curveClicked
            if event.button is MouseButton.LEFT and event.inaxes:
                if fig.canvas.widgetlock.locked():
                    return
                if not curveClicked:
                    return
                curveClicked = False
                if pointClicked:
                    pointClicked = False
                    return
                nearIndex = round(event.xdata)
                epsilon = 60
                if nearIndex < -epsilon:
                    return
                if nearIndex > len(ECG) + epsilon:
                    return
                local = ECG[max(nearIndex - epsilon, 0): nearIndex + epsilon + 1]
                x = np.argmax(local)
                nearIndex = x + nearIndex - epsilon
                if nearIndex < 0:
                    nearIndex = 0
                if nearIndex >= len(ECG):
                    nearIndex = len(ECG) - 1
                y = ECG[nearIndex]
                if nearIndex in peaks:
                    return
                insort(peaks, nearIndex)
                plt.plot(nearIndex, y, 'o', picker=True, color='r')
                plt.draw()

        def on_press(event):
            if event.key == 'enter':
                plt.close()

        plt.connect('key_press_event', on_press)
        plt.connect('button_press_event', on_click)
        plt.connect('pick_event', on_pick)
        plt.show()

    @classmethod
    def initializeAlignementPlot(cls, out: list[NDArray[np.float32], NDArray[np.float32]]) -> None:
        """Initializes the plot for aligning the pressure and flow channels."""
        figure, axis = plt.subplots(2)
        figure.suptitle("Realign the curves then press enter to proceed.")

        def draw(k: int) -> None:
            nonlocal out
            title = ["MAP: press left/right to move", "TCD: press down/up to move."]
            axis[k].clear()
            axis[k].plot(out[k], "b")
            axis[k].set_title(title[k])
            plt.draw()

        draw(0)
        draw(1)

        def on_key_press(event):
            nonlocal out
            units = ceil(out[0].size / 100)
            if event.key == 'right':
                out[0] = np.roll(out[0], units)
                draw(0)
            elif event.key == 'left':
                out[0] = np.roll(out[0], -units)
                draw(0)
            elif event.key == 'up':
                out[1] = np.roll(out[1], units)
                draw(1)
            elif event.key == 'down':
                out[1] = np.roll(out[1], -units)
                draw(1)
            elif event.key == 'enter':
                plt.close()

        figure.canvas.mpl_connect('key_press_event', on_key_press)
        plt.show()

    def makeCyclesFigure(self, simulation: list[float], interpolation: list[float],
                         params: tuple[float], cycle: int, selfManaged: bool) -> Figure:
        """Prepares the figure of a cycle to draw."""
        if selfManaged:
            figure = plt.figure()
        else:
            figure = Figure((self.dimension, self.dimension), dpi=80)
        ax = figure.add_subplot()
        ax.plot(simulation, 'b', label='TCD simulated', linestyle='dashed')
        ax.plot(interpolation, 'r', label='TCD interpolated')
        ax.title.set_text("Cycle number " + str(cycle))
        ax.set_xlabel('time: normalized to 100 units')
        ax.set_ylabel('flow rate (mL/min)')
        ax.legend()
        R, C, K, L, err = params
        axText = "R = " + str(R) + "\n" + "C = " + str(C) + "\n" + "K = " + str(K) + "\n" + "L = " + str(L) +\
            "\nE = " + str(err)
        ax.text(0.5, 0.95, axText, transform=ax.transAxes, fontsize=self._fontSize,
                verticalalignment='top', bbox=self._boxProp)
        return figure

    def drawCyclesPlot(self, simulation: list[float], interpolation: list[float],
                       params: tuple[int], cycle: int, selfManaged: bool) -> None:
        """Draws the plot of a cycle."""
        figure = self.makeCyclesFigure(simulation, interpolation, params, cycle, selfManaged)
        self._drawPlot(figure, selfManaged)

    def drawParametersPlot(self, parameters: np.ndarray[float],
                           variable: FittingVariables, selfManaged=True) -> plt.Figure:
        """Draws the plot of the selected RCKL parameter."""
        if selfManaged:
            figure = plt.figure()
        else:
            figure = Figure((self.dimension, self.dimension), dpi=80)
        ax = figure.add_subplot()
        size, _ = parameters.shape
        ax.plot(list(range(1, size + 1)), parameters[:, int(variable)])
        ax.title.set_text("Drawing the parameter " + VisualBackend.names[int(variable)])
        ax.set_xlabel("Cycle number")
        ax.set_ylabel(VisualBackend.names[int(variable)] + " in " + VisualBackend.units[int(variable)])
        self._drawPlot(figure, selfManaged)

    def getPeaks(self, ECG: NDArray[np.float32], thresholdFactor: float) -> list[int]:
        """Adjusts the peaks of the electrocardiogram."""
        MAX_AMPLITUDE: Final[np.float32] = np.max(ECG)
        threshold = thresholdFactor * MAX_AMPLITUDE
        peaks, _ = scipy.signal.find_peaks(ECG, threshold)
        peaks = peaks.tolist()
        self._plotPeaks(ECG, peaks)
        return peaks

    # TODO: Add the alignment function.

    @abstractmethod
    def handleException(self, message: str) -> None:
        """Method that specifies how to handle exceptions that happen due to
        IO and other issued.
        """
    @abstractmethod
    def alignChannels(self, fun1: NDArray[np.float32], fun2: NDArray[np.float32]) ->\
            tuple[NDArray[np.float32], NDArray[np.float32]]:
        pass

    @abstractmethod
    def _drawPlot(self, figure: Figure, selfManaged: bool) -> None:
        pass

    @abstractmethod
    def _plotPeaks(self, ECG: list[float], peaks: list[int]) -> None:
        pass


class _DefaultVisualBackend(VisualBackend):

    def handleException(self, message: str) -> None:
        print(message, file=sys.stderr)

    def alignChannels(self, fun1: NDArray[np.float32], fun2: NDArray[np.float32]) ->\
            tuple[NDArray[np.float32], NDArray[np.float32]]:
        out = [fun1, fun2]
        VisualBackend.initializeAlignementPlot(out)
        return (out[0], out[1])

    def _plotPeaks(self, ECG: list[float], peaks: list[int]) -> None:
        VisualBackend.initializePeaksPlot(ECG, peaks)

    def _drawPlot(self, figure: Figure, _selfManaged) -> None:
        figure.show()


class RCKL_Computation:
    """
    Computes the values of R, C, K and L in a Windkessel model.
    The function expects a file with three datavalues:
    ECG: the electrocardiogram.
    MAP: the pressure and
    TCD: the flow. The file should also contain the information
    on the sampling rate. If that information is not there,
    it can also by provided to the function manually.
    Once the class created, you can either display an array with R,C,K,L values
    in columns and cycle numbers in rows.
    You can plot individual cycles with simulated TCD
    and the interpolation of the measured one.
    You can also have any of the RCKL cycles recomputed
    based on approximations you feed to them.
    """
    def __init__(self, datapath: str, config: Config = Config.initialize()[0], order: tuple[int, int, int] = (0, 1, 2),
                 progressUpdater: ProgressUpdate = ProgressUpdate(),
                 plotBackend: VisualBackend = _DefaultVisualBackend()):
        # Declare the variables
        warnings.filterwarnings("ignore", message="delta_grad == 0.0")
        warnings.filterwarnings("ignore", message="Starting a Matplotlib GUI")
        self._numberOfCycles: int
        self._TCD_simulated: list[Callable]
        self._errorFunctions: list[Callable]
        self._gradients: list[Callable]
        self._TCD_interpolated: NDArray[np.float32]
        self._RCKLArray: NDArray[np.float32]
        self._errors: list[float]
        self._EXPECTED_BOUNDS: list[tuple]
        self._progressUpdater = progressUpdater
        # Get local variables from the config[0]

        first, second, third = order
        ALIGNMENT: Final[Alignment] =\
            Alignment(config.getVar(Config.DataCollectionParameters.ALIGNMENT))
        samplingRate: int =\
            config.getVar(Config.DataCollectionParameters.SAMPLING_RATE)
        THRESHOLD_FACTOR: Final[float] =\
            config.getVar(Config.DataCollectionParameters.THRESHOLD_FACTOR)

        INTERPOLATED_RATE: Final[int] =\
            config.getVar(Config.InterpolationParameters.INTERPOLATED_RATE)
        INTERPOLATION_TYPE: Final[InterpolationType] =\
            InterpolationType(config.getVar(Config.InterpolationParameters.INTERPOLATION_TYPE))
        HARMONIC_NUMBER: Final[int] =\
            config.getVar(Config.InterpolationParameters.HARMONIC_NUMBER)

        Cmin = config.getVar(Config.IterationParameters.EXPECTED_C_MIN)
        Cmax = config.getVar(Config.IterationParameters.EXPECTED_C_MAX)
        Kmin = config.getVar(Config.IterationParameters.EXPECTED_K_MIN)
        Kmax = config.getVar(Config.IterationParameters.EXPECTED_K_MAX)
        Lmin = config.getVar(Config.IterationParameters.EXPECTED_L_MIN)
        Lmax = config.getVar(Config.IterationParameters.EXPECTED_L_MAX)
        self._EXPECTED_BOUNDS: Final[list[tuple(int, int)]] =\
            [(Cmin, Cmax), (Kmin, Kmax), (Lmin, Lmax)]
        self._RANDOM_SEED: Final[int] =\
            config.getVar(Config.IterationParameters.RANDOM_SEED)
        self._ITERATIONS_NUMBER: Final[int] =\
            config.getVar(Config.IterationParameters.ITERATIONS_NUMBER)
        self._SEARCH_SIZE: Final[int] =\
            config.getVar(Config.IterationParameters.SEARCH_SIZE)

        R, G, B = config.getVar(
            Config.PlotingProperties.BOX_COLOR)
        faceColor = R / 256, G / 256, B / 256
        alpha = config.getVar(
            Config.PlotingProperties.ALPHA)
        boxProp: Final[dict] = {"boxstyle": 'round',
                                "facecolor": faceColor,
                                "alpha": alpha}

        self._significative_digits: Final[int] =\
            config.getVar(Config.PlotingProperties.SIGNIFICATIVE_DIGITS)
        fontSize: Final[int] = config.getVar(Config.PlotingProperties.FONTSIZE)

        # Initialize the lists
        self._errors = []
        self._errorFunctions = []
        self._gradients = []
        self._TCD_simulated = []

        self._plotBackend = plotBackend
        self._plotBackend.initializeProperties(fontSize, boxProp)

        # Extract the raw data, while handling possible errors occurring
        exception = None
        try:
            data = _Data(datapath)
            ECG = data.getCol(first)
            MAP = data.getCol(second)
            TCD = data.getCol(third)
        except IOError:
            exception = _IOError(datapath)
        except IndexError:
            exception = _WrongFormatException(datapath)
        except TypeError:
            exception = _WrongFormatException(datapath)
        else:
            if len(ECG) != len(MAP) or len(ECG) != len(TCD):
                exception = _WrongLengthException(datapath)
        if exception is not None:
            self._progressUpdater.requestStop()
            self._progressUpdater.stopRequested()
            raise exception

        cyclesMAP = []
        cyclesTCD = []

        try:
            samplingRate = data.getFreq()
        except KeyError:
            print("Warning, no sampling frequency found in the file: continuing with the default")
        timeVar = np.linspace(0, (ECG.size - 1) / samplingRate, ECG.size, dtype=np.float32)

        # Identify the cardiac cycles using the electrocardiogram data.
        peaks = self._plotBackend.getPeaks(ECG, THRESHOLD_FACTOR)
        self._numberOfCycles = len(peaks) - 1
        # Realign cycles
        if self._progressUpdater.stopRequested():
            return
        for iterations in range(self._numberOfCycles):
            peak = peaks[iterations]; peakNext = peaks[iterations + 1] + 1
            cycleMAP = np.copy(MAP[peak:peakNext])
            cycleTCD = np.copy(TCD[peak:peakNext])
            match ALIGNMENT:
                case Alignment.NONE:
                    pass
                case Alignment.AUTO:
                    timeShift = int(np.argmin(cycleMAP))
                    cycleMAP = np.roll(cycleMAP, -timeShift)
                    timeShift = int(np.argmin(cycleTCD))
                    cycleTCD = np.roll(cycleTCD, -timeShift)
                case Alignment.MANUAL:
                    timeShift = int(np.argmin(cycleMAP))
                    cycleMAP = np.roll(cycleMAP, -timeShift)
                    timeShift = int(np.argmin(cycleTCD))
                    cycleTCD = np.roll(cycleTCD, -timeShift)
                    cycleMAP, cycleTCD = self._plotBackend.alignChannels(cycleMAP, cycleTCD)
            if self._progressUpdater.stopRequested():
                return
            cyclesMAP.append(np.copy(cycleMAP))
            cyclesTCD.append(np.copy(cycleTCD))

        # Number of cardiac cycles identify, we can now initialize arrays to be saved:
        self._TCD_interpolated = np.zeros((self._numberOfCycles, INTERPOLATED_RATE), np.float32)
        self._RCKLArray = np.zeros((self._numberOfCycles, 4), float)

        # Declare temporary variables used in the loops.
        MAPTemp: NDArray[np.float32]; TCDTemp: NDArray[np.float32]
        MAPFourierAmplitude: NDArray[np.float32]; MAPFourierPhase: NDArray[np.float32]
        R: np.float32
        timeTemp: NDArray[np.float32]

        # Start to compute RCKL for all cycles
        self._progressUpdater.setNewProgress(self._numberOfCycles)
        for iterations in range(self._numberOfCycles):
            # Start by centering the two curves
            peak = peaks[iterations]
            peakNext = peaks[iterations + 1] + 1
            MAPTemp = cyclesMAP[iterations]
            TCDTemp = cyclesTCD[iterations]
            R = np.mean(MAPTemp) / np.mean(TCDTemp)
            MAPTemp = MAPTemp - np.mean(MAPTemp)
            TCDTemp = TCDTemp - np.mean(TCDTemp)

            # Compute the Fourier coefficients for MAP
            timeTemp = np.copy(timeVar[peak:peakNext])
            timeTemp = timeTemp - timeTemp[0]
            period = timeTemp[-1]
            MAPFourierAmplitude = np.zeros(HARMONIC_NUMBER, np.float32)
            MAPFourierPhase = np.zeros(HARMONIC_NUMBER, np.float32)
            for k in range(HARMONIC_NUMBER):
                (ampl, phase) = self._fourierCoeffs(MAPTemp, timeTemp, k)
                MAPFourierAmplitude[k] = ampl
                MAPFourierPhase[k] = phase

            # New time units:
            timeInterpolated = np.linspace(0.0, period, INTERPOLATED_RATE, dtype=np.float32)

            # Interpolate the flow:
            match INTERPOLATION_TYPE:
                case InterpolationType.SPLINE:
                    self._TCD_interpolated[iterations] = self._interp(TCDTemp, peak, peakNext, INTERPOLATED_RATE)
                case InterpolationType.FOURIER:
                    for k in range(HARMONIC_NUMBER):
                        (ampl, phase) = self._fourierCoeffs(TCDTemp, timeTemp, k)
                        for i in range(INTERPOLATED_RATE):
                            self._TCD_interpolated[iterations, i] += ampl *\
                                np.cos(2 * (k + 1) * np.pi * timeInterpolated[i] / period - phase)

            # Precompute the frequencies:
            omega = np.zeros(HARMONIC_NUMBER, np.float32)
            for k in range(HARMONIC_NUMBER):
                omega[k] = 2 * 60 * np.pi * (k + 1) / period

            # Define and store the functions we are looking for:
            def makeTCDSimulated(omega=np.copy(omega),
                                 MAPFourierAmplitude=np.copy(MAPFourierAmplitude),
                                 MAPFourierPhase=np.copy(MAPFourierPhase),
                                 timeInterpolated=np.copy(timeInterpolated),
                                 R=R, period=period) -> Callable:
                def TCDSimulated(C: np.float32, K: np.float32, L: np.float32)\
                        -> NDArray[np.float32]:
                    """Calculates the theoretical flow
                       for the given values CKL.  """
                    output = np.zeros(INTERPOLATED_RATE, np.complex64)
                    for k in range(HARMONIC_NUMBER):
                        NUM = R * (omega[k] * K * C + 1j * (pow(omega[k], 2) * L * C - 1))
                        DENOM = omega[k] * C * (K + R) + 1j * (pow(omega[k], 2) * L * C - 1)
                        Z = NUM / DENOM
                        harmonic: np.complex64
                        for i in range(INTERPOLATED_RATE):
                            harmonic = MAPFourierAmplitude[k] * \
                                np.exp(1j * (2 * np.pi * timeInterpolated[i] *
                                             (k + 1) / period - MAPFourierPhase[k]))
                            output[i] = output[i] + (1 / Z) * harmonic
                    return np.real(output)
                return TCDSimulated
            self._TCD_simulated.append(makeTCDSimulated())

            def makeError(TCDInterpolated=np.copy(self._TCD_interpolated[iterations, :]),
                          TCDSimulated=self._TCD_simulated[-1]) -> Callable:
                def error(params: np.ndarray,) -> float:
                    vec = TCDInterpolated - TCDSimulated(params[0], params[1], params[2])
                    sm = 0
                    for k in range(INTERPOLATED_RATE):
                        sm = sm + pow(vec[k], 2)
                    return float(sm)
                return error

            self._errorFunctions.append(makeError())

            def makeGradient(MAPFourierAmplitude=np.copy(MAPFourierAmplitude),
                             MAPFourierPhase=np.copy(MAPFourierPhase),
                             TCDInterpolated=np.copy(
                                 self._TCD_interpolated[iterations, :]),
                             timeInterpolated=np.copy(timeInterpolated),
                             period=period, R=R,
                             omega=np.copy(omega)) -> Callable:

                def gradient(params: np.ndarray) -> np.ndarray:
                    C = params[0]; K = params[1]; L = params[2]
                    harmonics = np.zeros((HARMONIC_NUMBER, INTERPOLATED_RATE), np.complex64)
                    for k in range(HARMONIC_NUMBER):
                        NUM = np.complex64(R * (omega[k] * K * C + 1j *
                                                (pow(omega[k], 2) * L * C - 1)))
                        DENOM = np.complex64(omega[k] * C * (K + R) + 1j *
                                             (pow(omega[k], 2) * L * C - 1))
                        Z = NUM / DENOM
                        for i in range(INTERPOLATED_RATE):
                            harmonics[k, i] = MAPFourierAmplitude[k] * 1 / Z * \
                                np.exp(1j * (2 * (k + 1) * np.pi *
                                             timeInterpolated[i] /
                                             period - MAPFourierPhase[k]))
                    distance = np.real(np.sum(harmonics, 0) - TCDInterpolated)
                    diffC = np.zeros(INTERPOLATED_RATE, np.complex64)
                    diffK = np.zeros(INTERPOLATED_RATE, np.complex64)
                    diffL = np.zeros(INTERPOLATED_RATE, np.complex64)
                    DENOM_ARRAY = np.zeros(HARMONIC_NUMBER, np.complex64)
                    for k in range(HARMONIC_NUMBER):
                        DENOM_ARRAY[k] = pow(omega[k] * K * C + 1j * (pow(omega[k], 2) * L * C - 1), 2)
                    for k in range(HARMONIC_NUMBER):
                        NUM = ((omega[k] * (K + R)) + 1j * pow(omega[k], 2) * L) *\
                              (omega[k] * K * C + 1j * (pow(omega[k], 2) * L * C - 1)) / R -\
                              (omega[k] * K + 1j * pow(omega[k], 2) * L) *\
                              (omega[k] * C * (K + R) + 1j * (pow(omega[k], 2) * L * C - 1)) / R
                        factor = NUM / DENOM_ARRAY[k]
                        for i in range(INTERPOLATED_RATE):
                            diffC[i] = diffC[i] + factor *\
                                MAPFourierAmplitude[k] * np.exp(1j * (2 * np.pi * (k + 1) * timeInterpolated[i] /
                                                                      period - MAPFourierPhase[k]))
                    for k in range(HARMONIC_NUMBER):
                        NUM = -pow(omega[k] * C, 2)
                        factor = NUM / DENOM_ARRAY[k]
                    for i in range(INTERPOLATED_RATE):
                        diffK[i] = diffK[i] + factor *\
                            MAPFourierAmplitude[k] * np.exp(1j * (2 * np.pi * (k + 1) *
                                                                  timeInterpolated[i] / period - MAPFourierPhase[k]))
                    for k in range(HARMONIC_NUMBER):
                        NUM = -1j * pow(omega[k], 3) * pow(C, 2)
                        factor = NUM / DENOM_ARRAY[k]
                    for i in range(INTERPOLATED_RATE):
                        diffL[i] = diffL[i] + factor *\
                            MAPFourierAmplitude[k] * np.exp(1j * (2 * np.pi * (k + 1) *
                                                                  timeInterpolated[i] / period - MAPFourierPhase[k]))
                    out = np.zeros(3, float)
                    out[0] = np.sum(np.real(diffC) * distance)
                    out[1] = np.sum(np.real(diffK) * distance)
                    out[2] = np.sum(np.real(diffL) * distance)
                    return 2 * out
                return gradient

            self._gradients.append(makeGradient())
            initialEstimate = list(self._presearchRCKL(self._errorFunctions[-1]))
            (C, K, L) = self._findCKL(self._errorFunctions[-1], self._gradients[-1], initialEstimate)
            if iterations > 0:
                prevC = self._RCKLArray[iterations - 1, 1]
                prevK = self._RCKLArray[iterations - 1, 2]
                prevL = self._RCKLArray[iterations - 1, 3]
                m = self._findCKL(self._errorFunctions[-1], self._gradients[-1], [prevC, prevK, prevL])
                if self._errorFunctions[-1]([C, K, L]) > self._errorFunctions[-1](m):
                    C = m[0]
                    K = m[1]
                    L = m[2]
            self._RCKLArray[iterations, 0] = R
            self._RCKLArray[iterations, 1] = C
            self._RCKLArray[iterations, 2] = K
            self._RCKLArray[iterations, 3] = L
            self._errors.append(self._errorFunctions[-1]([C, K, L]))
            if self._progressUpdater.stopRequested():
                return
            self._progressUpdater.tick()

    def results(self) -> NDArray[np.float32]:
        """
        Returns the results of the RCKL calculations
            Parameters:
               None
            Returns:
               RCKL_Array (NDArray[np.float32]): An Nx4 array, with N the number of cycles and the columns
               being in the order R, C, K and L.
        """
        return np.copy(self._RCKLArray)

    def drawCycle(self, cycle: int, selfManaged=True) -> None:
        """Draws the cycle of decided number."""
        simulation, interpolation, params = self._preparePlot(cycle)
        self._plotBackend.drawCyclesPlot(simulation, interpolation, params, cycle, selfManaged)

    def drawParameters(self, variable: FittingVariables, selfManaged: bool = False) -> None:
        """
        Draws the evolution of a given parameter over cycles.
            Parameters:
                n (int): The number of the parameter to draw: 0:R 1:C 2:K 3:L
            Returns:
                None

        """
        self._plotBackend.drawParametersPlot(self._RCKLArray, variable, selfManaged)

    def refine(self) -> None:
        """
        Refines the calculation of the fits by using the values of the previous wave as
        parameters for the next one.
        """
        self._progressUpdater.setNewProgress(2 * self._numberOfCycles - 2)
        for iterations in range(self._numberOfCycles - 1):
            position = self._numberOfCycles - iterations - 2
            Cprev = self._RCKLArray[position + 1, 1]
            Kprev = self._RCKLArray[position + 1, 2]
            Lprev = self._RCKLArray[position + 1, 3]
            m = self._findCKL(self._errorFunctions[iterations], self._gradients[iterations],
                              [Cprev, Kprev, Lprev], self._EXPECTED_BOUNDS)
            newError = self._errorFunctions[position](m)
            if self._errors[position] > newError:
                self._RCKLArray[position, 1] = m[0]
                self._RCKLArray[position, 2] = m[1]
                self._RCKLArray[position, 3] = m[2]
                self._errors[position] = newError
            if self._progressUpdater.stopRequested():
                return
            self._progressUpdater.tick()
        for iterations in range(self._numberOfCycles - 1):
            position = iterations + 1
            Cprev = self._RCKLArray[position - 1, 1]
            Kprev = self._RCKLArray[position - 1, 2]
            Lprev = self._RCKLArray[position - 1, 3]
            m = self._findCKL(self._errorFunctions[iterations], self._gradients[iterations],
                              [Cprev, Kprev, Lprev], self._EXPECTED_BOUNDS)
            newError = self._errorFunctions[position](m)
            if self._errors[position] > newError:
                self._RCKLArray[position, 1] = m[0]
                self._RCKLArray[position, 2] = m[1]
                self._RCKLArray[position, 3] = m[2]
                self._errors[position] = newError
            if self._progressUpdater.stopRequested():
                return
            self._progressUpdater.tick()

    def recalculateCycle(self, n: int, C0: float, K0: float, L0: float, bounds: list[tuple] = None,
                         graph: bool = False) -> tuple[float, float, float]:
        """
        Racalculates the cycle with changed initial values and bounds.
            Parameters:
                n (int): Number of the cycle between 1 and the total number of cycles.
                C0 (float): New initial value for the compliance.
                K0 (float): New initial value for the viscoelasticity.
                L0 (float): New initial value for the inertance.
                bounds (list[tuple]) (Optional): Optional new bounds for the recalculation. If none are given
                                                 initial default bounds are selected.
                graph (boolean) (Optional): Boolean that determines whether the new fit will be drawn or not.
                                            Defaults to False.
             Returns:
                (C, K, L) the three new recalculated values.
        """
        if not bounds:
            bounds = self._EXPECTED_BOUNDS.copy()
        C, K, L = self._findCKL(self._errorFunctions[n - 1], self._gradients[n - 1], [C0, K0, L0], bounds)

        self._errors[n - 1] = self._errorFunctions[n - 1]([C, K, L])
        self._RCKLArray[n - 1, 1] = C
        self._RCKLArray[n - 1, 2] = K
        self._RCKLArray[n - 1, 3] = L
        if graph:
            self.drawCycle(n)
        return C, K, L

    def exportCSV(self, filename: str) -> bool:
        """
        Export the calculated values to a csv file.
            Parameters:
                filename (str): Path to the file. If the string is invalid, prints a warning to the
                stderr.
            Returns:
                None
        """
        try:
            np.savetxt(filename, self._RCKLArray, delimiter=',',
                       header='Resistance, Compliance, Viscoelasticity, Inertance')
        except IOError:
            message = "Couldn't write to the file " + filename +\
                ". Check that the path is correct and accessible from the system"
            self._plotBackend.handleException(message)
            return False
        return True

    def saveGraphs(self, filename: str) -> bool:
        """ Saves all the graphs as a pdf file.
            Parameters:
                filename (str): Path to the pdf file where graphs are to be saved.
            Returns:
                None
        """
        try:
            with PdfPages(filename) as pdf:
                for cycle in range(self._numberOfCycles):
                    simulation, interpolation, params = self._preparePlot(cycle + 1)
                    figure = self._plotBackend.makeCyclesFigure(simulation, interpolation, params, cycle + 1, False)
                    pdf.savefig(figure)
        except IOError:
            message = "Couldn't write to the file " + filename +\
                ". Check that the path is correct and accessible from the system"

            self._plotBackend.handleException(message)
            return False
        return True

    def getNumberOfCycles(self) -> int:
        """Shows the number of the computed cycles."""
        return self._numberOfCycles

    @classmethod
    def _realign(cls, fun1: NDArray[np.float32], fun2: NDArray[np.float32]) \
            -> tuple[NDArray[np.float32], NDArray[np.float32]]:
        ret1 = np.copy(fun1)
        ret2 = np.copy(fun2)
        figure, axis = plt.subplots(2)
        figure.suptitle("Realign the curves then press enter to proceed.")

        def draw(k: int) -> None:
            title = ["MAP: press left/right to move", "TCD: press down/up to move."]
            toPlot = [ret1, ret2]
            axis[k].clear()
            axis[k].plot(toPlot[k], "b")
            axis[k].set_title(title[k])
            plt.draw()

        draw(0)
        draw(1)

        def on_key_press(event):
            nonlocal ret1, ret2
            units = ceil(ret1.size / 100)
            if event.key == 'right':
                ret1 = np.roll(ret1, units)
                draw(0)
            elif event.key == 'left':
                ret1 = np.roll(ret1, -units)
                draw(0)
            elif event.key == 'up':
                ret2 = np.roll(ret2, units)
                draw(1)
            elif event.key == 'down':
                ret2 = np.roll(ret2, -units)
                draw(1)
            elif event.key == 'enter':
                plt.close()

        figure.canvas.mpl_connect('key_press_event', on_key_press)

        plt.show()

        return ret1, ret2

    @classmethod
    def _interp(cls, data: np.ndarray, lower: int, upper: int, interpolatedRate: int) -> np.ndarray:
        """Interpolates the data with the cubic spline method."""
        interpolator = CubicSpline(np.arange(lower, upper), data)
        newx = np.linspace(lower, upper, interpolatedRate)
        newy = interpolator(newx)
        return newy

    @classmethod
    def _integrate(cls, func: Callable, data: np.ndarray,
                   timeScale: np.ndarray, n: int):
        sm: np.float32 = np.float32(0)
        delta_t: np.float32
        period: np.float32 = timeScale[-1] - timeScale[0]
        for k in range(data.size - 1):
            delta_t = timeScale[k + 1] - timeScale[k]
            upper = data[k + 1] * func(2 * (n + 1) * np.pi * timeScale[k + 1] / period)
            lower = data[k] * func(2 * (n + 1) * np.pi * timeScale[k] / period)
            sm = sm + (upper + lower) / 2 * delta_t
        return (2 / period) * sm

    @classmethod
    def _fourierCoeffs(cls, data: np.ndarray, timeScale: np.ndarray, n: int) -> tuple[np.float32, np.float32]:
        """Calculates the Fourier coefficients from the input data."""
        A = cls._integrate(np.cos, data, timeScale, n)
        B = cls._integrate(np.sin, data, timeScale, n)
        C = np.sqrt(pow(A, 2) + pow(B, 2))
        phi = np.arctan2(B, A)
        return (C, phi)

    def _findCKL(self, error: Callable, gradient: Callable,
                 initialEstimate: list[float],
                 expectedBounds: list = None) -> tuple[float, float, float]:
        if expectedBounds is None:
            expectedBounds = self._EXPECTED_BOUNDS
        optim = minimize(error, initialEstimate, method='Trust-constr',
                         jac=gradient, bounds=expectedBounds)
        C = optim.x[0]
        K = optim.x[1]
        L = optim.x[2]
        if C < 0:
            C = expectedBounds[0][0]
        if K < 0:
            K = expectedBounds[1][0]
        if L < 0:
            L = expectedBounds[2][0]
        return (C, K, L)

    def _preparePlot(self, cycle: int) -> tuple[list[float], list[float], tuple[float]]:
        """Formats the data for the figure to draw."""
        execute = True
        execute = execute and isinstance(cycle, int)
        execute = execute and cycle >= 1
        execute = execute and cycle <= self._numberOfCycles
        if not execute:
            print("You must enter a number between 1 and number of cycles",
                  file=sys.stderr)
            return
        C = self._RCKLArray[cycle - 1, 1]
        K = self._RCKLArray[cycle - 1, 2]
        L = self._RCKLArray[cycle - 1, 3]
        simulation = self._TCD_simulated[cycle - 1](C, K, L)
        interpolation = self._TCD_interpolated[cycle - 1, :]
        R, C, K, L, err = self._format(cycle - 1)
        return simulation, interpolation, (R, C, K, L, err)

    def _format(self, cycle: int) -> tuple[float, float, float, float]:
        R = _significantDigitsRound(self._RCKLArray[cycle, 0], self._significative_digits)
        C = _significantDigitsRound(self._RCKLArray[cycle, 1], self._significative_digits)
        K = _significantDigitsRound(self._RCKLArray[cycle, 2], self._significative_digits)
        L = _significantDigitsRound(self._RCKLArray[cycle, 3], self._significative_digits)
        err = round(self._errors[cycle])
        return R, C, K, L, err

    def _presearchRCKL(self, func: Callable) -> tuple[float, float, float]:
        """Searches the initial approximation of RCKL, by trying equidistant values"""
        seed(self._RANDOM_SEED)
        Cmin = self._EXPECTED_BOUNDS[0][0]
        Cmax = self._EXPECTED_BOUNDS[0][1]
        Kmin = self._EXPECTED_BOUNDS[1][0]
        Kmax = self._EXPECTED_BOUNDS[1][1]
        Lmin = self._EXPECTED_BOUNDS[2][0]
        Lmax = self._EXPECTED_BOUNDS[2][1]
        Cout, Kout, Lout = (Cmin + Cmax) / 2, (Kmin + Kmax) / 2, (Lmax + Lmin) / 2
        M = func([Cout, Kout, Lout])
        CInterval = np.linspace(Cmin, Cmax, self._SEARCH_SIZE)
        KInterval = np.linspace(Kmin, Kmax, self._SEARCH_SIZE)
        LInterval = np.linspace(Lmin, Lmax, self._SEARCH_SIZE)
        for _ in range(self._ITERATIONS_NUMBER):
            x, y, z = randint(0, self._SEARCH_SIZE - 1), \
                randint(0, self._SEARCH_SIZE - 1), \
                randint(0, self._SEARCH_SIZE - 1)
            C = CInterval[x]
            K = KInterval[y]
            L = LInterval[z]
            var = func([C, K, L])
            if M > var:
                M = var
                Cout = C
                Kout = K
                Lout = L

        return Cout, Kout, Lout


class _Data:
    """This class holds the data loaded from the mat files."""

    __mat: dict

    def __init__(self, filepath):
        self.__mat = scipy.io.loadmat(filepath)

    def getCol(self, columnNumber: int) -> NDArray[np.float32]:
        """Return the array corresponding to the given data column."""
        datastart = int(self.__mat["datastart"][columnNumber][0]) - 1
        dataend = int(self.__mat["dataend"][columnNumber][0])
        column = self.__mat["data"][0, datastart:dataend]
        return column

    def getFreq(self) -> int:
        """Returns the sampling frequency from the data"""
        return int(self.__mat["samplerate"][0][0])


def _significantDigitsRound(x: float, numberOfDigits: int) -> float:
    if x == 0:
        return float(0)
    return round(x, numberOfDigits - 1 - int(np.floor(np.log10(abs(x)))))
