
import wx
from config import Config


class _FloatingValidator(wx.Validator):
    _acceptedKeys = [*range(48, 58)]
    _acceptedKeys.append(8)
    _acceptedKeys.append(127)
    _acceptedKeys.append(ord('-'))
    _acceptedKeys.append(ord('.'))
    _acceptedKeys.append(ord('e'))

    @classmethod
    def checkAccepted(cls, key: int) -> bool:
        return key in cls._acceptedKeys

    def __init__(self):
        super().__init__()
        self.Bind(wx.EVT_CHAR, self._OnChar)

    def Clone(self):
        return _FloatingValidator()

    def Validate(self, parent):
        text = parent.GetValue()
        try:
            float(text)
            return True
        except ValueError:
            return False

    def _OnChar(self, event: wx.Event):
        keycode = int(event.GetUnicodeKey())
        if not self.checkAccepted(keycode):
            return
        event.Skip()

    def TransferToWindow(self):
        return True

    def TransferFromWindow(self):
        return True

class CycleParametersDialog(wx.Dialog):
    def __init__(self, RCKL: tuple[float, float, float], *arg, **kwarg):
        super().__init__(*arg, *kwarg)
        self.SetTitle("Recalculation parameters")
        C, K, L = RCKL
        config, code = Config.initialize()

        Cmin = config.getVar(Config.IterationParameters.EXPECTED_C_MIN)
        Cmax = config.getVar(Config.IterationParameters.EXPECTED_C_MAX)
        Kmin = config.getVar(Config.IterationParameters.EXPECTED_K_MIN)
        Kmax = config.getVar(Config.IterationParameters.EXPECTED_K_MAX)
        Lmin = config.getVar(Config.IterationParameters.EXPECTED_L_MIN)
        Lmax = config.getVar(Config.IterationParameters.EXPECTED_L_MAX)

        mainSizer = wx.BoxSizer(wx.VERTICAL)

        title = wx.StaticText(self, wx.ID_ANY, "Enter new initial parameters.")
        mainSizer.Add(title, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        mainSizer.Add((0, 20), 0, 0, 0)

        valuesGrid = wx.GridSizer(7, 3, 2, 4)
        mainSizer.Add(valuesGrid, 0, wx.EXPAND, 0)

        Clabel = wx.StaticText(self, wx.ID_ANY, "Compliance")
        valuesGrid.Add(Clabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Klabel = wx.StaticText(self, wx.ID_ANY, "Viscoelasticity")
        valuesGrid.Add(Klabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Llabel = wx.StaticText(self, wx.ID_ANY, "Inductance")
        valuesGrid.Add(Llabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Cminlabel = wx.StaticText(self, wx.ID_ANY, "min")
        valuesGrid.Add(Cminlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Kminlabel = wx.StaticText(self, wx.ID_ANY, "min")
        valuesGrid.Add(Kminlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Lminlabel = wx.StaticText(self, wx.ID_ANY, "min")
        valuesGrid.Add(Lminlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Cmin = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Cmin.SetValue(str(Cmin))
        valuesGrid.Add(self.Cmin, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Kmin = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Kmin.SetValue(str(Kmin))
        valuesGrid.Add(self.Kmin, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Lmin = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Lmin.SetValue(str(Lmin))
        valuesGrid.Add(self.Lmin, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Cmaxlabel = wx.StaticText(self, wx.ID_ANY, "max")
        valuesGrid.Add(Cmaxlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Kmaxlabel = wx.StaticText(self, wx.ID_ANY, "max")
        valuesGrid.Add(Kmaxlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Lmaxlabel = wx.StaticText(self, wx.ID_ANY, "max")
        valuesGrid.Add(Lmaxlabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Cmax = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Cmax.SetValue(str(Cmax))
        valuesGrid.Add(self.Cmax, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Kmax = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Kmax.SetValue(str(Kmax))
        valuesGrid.Add(self.Kmax, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Lmax = wx.TextCtrl(self, wx.ID_ANY, "", validator=_FloatingValidator())
        self.Lmax.SetValue(str(Lmax))
        valuesGrid.Add(self.Lmax, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Cvallabel = wx.StaticText(self, wx.ID_ANY, "initial guess")
        valuesGrid.Add(Cvallabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Kvallabel = wx.StaticText(self, wx.ID_ANY, "initial guess")
        valuesGrid.Add(Kvallabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        Lvallabel = wx.StaticText(self, wx.ID_ANY, "initial guess")
        valuesGrid.Add(Lvallabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Cval = wx.TextCtrl(self, wx.ID_ANY, "")
        self.Cval.SetValue(str(C))
        valuesGrid.Add(self.Cval, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Kval = wx.TextCtrl(self, wx.ID_ANY, "")
        self.Kval.SetValue(str(K))
        valuesGrid.Add(self.Kval, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.Lval = wx.TextCtrl(self, wx.ID_ANY, "")
        self.Lval.SetValue(str(L))
        valuesGrid.Add(self.Lval, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        mainSizer.Add((0, 20), 0, 0, 0)

        sizer_2 = wx.StdDialogButtonSizer()
        mainSizer.Add(sizer_2, 0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.button_OK = wx.Button(self, wx.ID_OK, "")
        self.button_OK.SetDefault()
        sizer_2.AddButton(self.button_OK)

        self.button_CANCEL = wx.Button(self, wx.ID_CANCEL, "")
        sizer_2.AddButton(self.button_CANCEL)

        sizer_2.Realize()

        self.SetSizer(mainSizer)
        mainSizer.Fit(self)

        self.SetAffirmativeId(self.button_OK.GetId())
        self.SetEscapeId(self.button_CANCEL.GetId())

        self.Layout()

    def Validate(self):
        result = super().Validate()
        Cmin = float(self.Cmin.GetValue())
        Cmax = float(self.Cmax.GetValue())
        Cval = float(self.Cval.GetValue())
        if Cval < Cmin or Cval > Cmax:
            wx.MessageBox("The initial guess for compliance " +
                          "has to be a number between the minimum and maximum", "Validation Failed",
                          wx.OK | wx.ICON_WARNING)
            return False

        Kmin = float(self.Kmin.GetValue())
        Kmax = float(self.Kmax.GetValue())
        Kval = float(self.Kval.GetValue())
        if Kval < Kmin or Kval > Kmax:
            wx.MessageBox("The initial guess for viscoelasticity " +
                          "has to be a number between the minimum and maximum", "Validation Failed",
                          wx.OK | wx.ICON_WARNING)
            return False

        Lmin = float(self.Lmin.GetValue())
        Lmax = float(self.Lmax.GetValue())
        Lval = float(self.Lval.GetValue())

        if Lval < Lmin or Lval > Lmax:
            wx.MessageBox("The initial guess for inductance " +
                         "has to be a number between the minimum and maximum", "Validation Failed",
                         wx.OK | wx.ICON_WARNING)
            return False

        return result
