#!/usr/bin/env python3
"""
Gui front end launcher for the RCKL application.
"""
import wx
from buttonDrag import OrderWidget
from preferencesDlg import PreferencesDialog
from config import Config
from analysisPanel import AnalysisPanel


class MainFrame(wx.Frame):
    "Main Frame class: This defines how the GUI elements will look."
    def __init__(self, *args, **kwds):
        # begin wxGlade: MainFrame.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self._orderWidget = OrderWidget(self)
        self._orderWidget.createNewUnit("Electrocardiogram")
        self._orderWidget.createNewUnit("Pressure")
        self._orderWidget.createNewUnit("Flow")
        self.SetSize((960, 1043))
        self.SetTitle("RCKL Program")

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.options = wx.Button(self, wx.ID_PREFERENCES, "")
        self.options.Bind(wx.EVT_BUTTON, self._onClickPreferences)
        self.mainSizer.Add(self.options, 0, wx.ALIGN_RIGHT | wx.RIGHT, 0)
        self.mainSizer.Add((20, 50), 0, 0, 0)

        label_1 = wx.StaticText(self, wx.ID_ANY, "Channel Order (Drag to change)", style=wx.ALIGN_CENTER_HORIZONTAL)
        self.mainSizer.Add(label_1, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        self.mainSizer.Add((20, 5), 0, 0, 0)
        self.mainSizer.Add(self._orderWidget, 0, wx.ALIGN_CENTER, 0)
        self.mainSizer.Add((20, 96), 0, 0, 0)

        label_3 = wx.StaticText(self, wx.ID_ANY, "Path to data. (Click to change)")
        self.mainSizer.Add(label_3, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.path = wx.TextCtrl(self, style=wx.TE_READONLY)
        # self.path.Disable()
        self.path.SetMinSize((400, 33))
        self.path.Bind(wx.EVT_LEFT_DOWN, self._onClickBrowse)
        self.mainSizer.Add(self.path, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.mainSizer.Add((20, 50), 0, 0, 0)

        self.startButton = wx.Button(self, wx.ID_ANY, "Start the program")
        self.startButton.SetMinSize((250, 50))
        self.startButton.Bind(wx.EVT_BUTTON, self._onClickStart)
        self.mainSizer.Add(self.startButton, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.SetSizer(self.mainSizer)
        self.Layout()

    def _onClickBrowse(self, _event):
        self.path.Disable()
        openFileDialog = wx.FileDialog(self, "Open", "", "", "", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        result = openFileDialog.ShowModal()
        if result == wx.ID_OK:
            path = openFileDialog.GetPath()
            self.path.SetValue(path)
        self.path.Enable()
        openFileDialog.Destroy()

    def _onClickStart(self, _event):
        path = self.path.GetValue()
        order = self._orderWidget.getOrder()
        panel = AnalysisPanel(self, path)
        self.mainSizer.Clear(True)
        self.mainSizer.Add(panel, 0, wx.ALIGN_CENTER_HORIZONTAL)
        self.Layout()
        panel.loadData(order)

    def _onClickPreferences(self, _event):
        dlg = PreferencesDialog(self)
        result = dlg.ShowModal()
        if result == wx.ID_OK:
            dictionary: dict = {}
            dictionary[Config.DataCollectionParameters.ALIGNMENT.value] =\
                dlg.alignment.GetCurrentSelection()
            dictionary[Config.DataCollectionParameters.SAMPLING_RATE] =\
                int(dlg.SamplingRate.GetValue())
            dictionary[Config.DataCollectionParameters.THRESHOLD_FACTOR] =\
                float(dlg.threshold.GetValue() / 100)

            dictionary[Config.InterpolationParameters.INTERPOLATED_RATE] =\
                int(dlg.interpolationPoints.GetValue())
            dictionary[Config.InterpolationParameters.INTERPOLATION_TYPE] =\
                dlg.interpolationType.GetCurrentSelection()
            dictionary[Config.InterpolationParameters.HARMONIC_NUMBER] =\
                int(dlg.numberOfHarmonics.GetValue())

            dictionary[Config.IterationParameters.EXPECTED_C_MIN] =\
                float(dlg.cmin.GetValue())
            dictionary[Config.IterationParameters.EXPECTED_C_MAX] =\
                float(dlg.cmax.GetValue())
            dictionary[Config.IterationParameters.EXPECTED_K_MIN] =\
                float(dlg.kmin.GetValue())
            dictionary[Config.IterationParameters.EXPECTED_K_MAX] =\
                float(dlg.kmax.GetValue())
            dictionary[Config.IterationParameters.EXPECTED_L_MIN] =\
                float(dlg.lmin.GetValue())
            dictionary[Config.IterationParameters.EXPECTED_L_MAX] =\
                float(dlg.lmax.GetValue())

            dictionary[Config.IterationParameters.ITERATIONS_NUMBER] =\
                int(dlg.numberOfIterations.GetValue())
            dictionary[Config.IterationParameters.SEARCH_SIZE] =\
                int(dlg.searchSize.GetValue())
            dictionary[Config.IterationParameters.RANDOM_SEED] =\
                int(dlg.randomSeed.GetValue())

            dictionary[Config.PlotingProperties.SIGNIFICATIVE_DIGITS] =\
                int(dlg.significativeDigits.GetValue())
            dictionary[Config.PlotingProperties.FONTSIZE] =\
                int(dlg.fontSize.GetValue())
            dictionary[Config.PlotingProperties.ALPHA] = \
                float(dlg.alpha.GetValue() / 100)
            color = dlg.color.GetColour()
            R, G, B = color.GetRed(), color.GetGreen(), color.GetBlue()
            dictionary[Config.PlotingProperties.BOX_COLOR] = (R, G, B)

            config = Config(dictionary)
            config.saveConfig()
        dlg.Destroy()


class RCKLApp(wx.App):
    "The class that starts the application."
    def __init__(self, x: int):
        super().__init__(x)
        self.mainFrame: MainFrame

    def OnInit(self):
        """Override of the default wx.App OnInit method."""
        self.mainFrame = MainFrame(None, wx.ID_ANY, "")
        self.SetTopWindow(self.mainFrame)
        self.mainFrame.Show()
        return True


if __name__ == "__main__":
    mainApp = RCKLApp(0)
    mainApp.MainLoop()
