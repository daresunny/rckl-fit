import wx
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('WXAgg')


from numpy import linspace, sin
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from matplotlib.backends.backend_wx import NavigationToolbar2Wx
from matplotlib.figure import Figure
from config import Config

class _NumericValidator(wx.Validator):
    def __init__(self):
        super().__init__()
        self.Bind(wx.EVT_CHAR, self._OnChar)

    def Clone(self):
        return _NumericValidator()

    def Validate(self, parent: wx.TextCtrl):
        return parent.GetValue().isdigit()

    def _OnChar(self, event):
        keycode = int(event.GetUnicodeKey())
        if (keycode < 48 or keycode > 57) and keycode != 8 and keycode != 127:
            return
        event.Skip()

    def TransferToWindow(self):
        return True

    def TransferFromWindow(self):
        return True

class _FloatingValidator(wx.Validator):
    _acceptedKeys = [*range(48, 58)]
    _acceptedKeys.append(8)
    _acceptedKeys.append(127)
    _acceptedKeys.append(ord('-'))
    _acceptedKeys.append(ord('.'))
    _acceptedKeys.append(ord('e'))

    @classmethod
    def checkAccepted(cls, key: int) -> bool:
        return key in cls._acceptedKeys

    def __init__(self):
        super().__init__()
        self.Bind(wx.EVT_CHAR, self._OnChar)

    def Clone(self):
        return _FloatingValidator()

    def Validate(self, parent):
        text = parent.GetValue()
        try:
            float(text)
            return True
        except ValueError:
            return False

    def _OnChar(self, event: wx.Event):
        keycode = int(event.GetUnicodeKey())
        if not self.checkAccepted(keycode):
            return
        event.Skip()

    def TransferToWindow(self):
        return True

    def TransferFromWindow(self):
        return True

class PreferencesDialog(wx.Dialog):
    def __init__(self, *args, **kwds):
        # begin wxGlade: Preferences.__init__
        kwds["style"] = kwds.get("style", 0) | wx.DEFAULT_DIALOG_STYLE
        wx.Dialog.__init__(self, *args, **kwds)
        self.SetTitle("dialog")
        self._fig = None

        sizer_1 = wx.BoxSizer(wx.VERTICAL)

        self.notebook = wx.Notebook(self, wx.ID_ANY)
        self.notebook.SetMinSize((800, 400))
        sizer_1.Add(self.notebook, 1, wx.EXPAND, 0)

        self.collectionParameters = wx.Panel(self.notebook, wx.ID_ANY)
        self.notebook.AddPage(self.collectionParameters, "Data Collection Parameters")

        collectionParametersSizer = wx.BoxSizer(wx.VERTICAL)

        samplingRate = wx.StaticText(self.collectionParameters, wx.ID_ANY, "Sampling Rate",
                                     style=wx.ALIGN_CENTER_HORIZONTAL)
        collectionParametersSizer.Add(samplingRate, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        collectionParametersSizer.Add((20, 10), 0, 0, 0)

        self.SamplingRate = wx.TextCtrl(self.collectionParameters, wx.ID_ANY, "", validator=_NumericValidator())
        self.SamplingRate.SetMaxLength(12)
        self.SamplingRate.Bind(wx.EVT_TEXT, self._validate)
        self.SamplingRate.SetToolTip("Sampling rate of the data in Hertz. (Integer value)")

        collectionParametersSizer.Add(self.SamplingRate, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        collectionParametersSizer.Add((20, 40), 0, 0, 0)

        label_2 = wx.StaticText(self.collectionParameters, wx.ID_ANY, "Alignment")
        collectionParametersSizer.Add(label_2, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        collectionParametersSizer.Add((20, 10), 0, 0, 0)

        self.alignment = wx.Choice(self.collectionParameters, wx.ID_ANY,
                                   choices=["None", "Automatic", "Manual"])
        self.alignment.SetToolTip("Choose whether you want to align flow " +
                                  "and pressure automatically, manually or not at all.")
        self.alignment.SetSelection(0)
        collectionParametersSizer.Add(self.alignment, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        collectionParametersSizer.Add((20, 40), 0, 0, 0)

        label_3 = wx.StaticText(self.collectionParameters, wx.ID_ANY, "Electrocardiogram Threshold")
        collectionParametersSizer.Add(label_3, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        collectionParametersSizer.Add((20, 10), 0, 0, 0)

        self.threshold = wx.Slider(self.collectionParameters, wx.ID_ANY, 0, 0, 100)
        self.threshold.SetMinSize((200, 32))
        self.threshold.SetToolTip("Threshold for detecting electrocardiogram " +
                                  "peaks. The higher the value, " +
                                  "the less false positives, the lower the less false negatives.")
        collectionParametersSizer.Add(self.threshold, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.LEFT, 0)

        self.interpolationParameters = wx.Panel(self.notebook, wx.ID_ANY)
        self.notebook.AddPage(self.interpolationParameters, "Interpolation Parameters")

        interpolationParametersSizer = wx.BoxSizer(wx.VERTICAL)

        label_4 = wx.StaticText(self.interpolationParameters, wx.ID_ANY, "How many interpolation points?")
        interpolationParametersSizer.Add(label_4, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        interpolationParametersSizer.Add((20, 10), 0, 0, 0)

        self.interpolationPoints = wx.TextCtrl(self.interpolationParameters, wx.ID_ANY, "", validator=_NumericValidator())
        self.interpolationPoints.SetMaxLength(12)
        self.interpolationPoints.Bind(wx.EVT_TEXT, self._validate)
        self.interpolationPoints.SetToolTip("Number of points for interpolating the flow over one cycle, " +
                                            "using the measured values.")
        interpolationParametersSizer.Add(self.interpolationPoints, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        interpolationParametersSizer.Add((20, 40), 0, 0, 0)

        label_5 = wx.StaticText(self.interpolationParameters, wx.ID_ANY, "Interpolation Type")
        interpolationParametersSizer.Add(label_5, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        interpolationParametersSizer.Add((20, 10), 0, 0, 0)

        self.interpolationType = wx.Choice(self.interpolationParameters, wx.ID_ANY,
                                           choices=["Cubic Spline", "Fourier Series"])
        self.interpolationType.SetToolTip("The method for interpolating.")
        self.interpolationType.SetSelection(0)
        interpolationParametersSizer.Add(self.interpolationType, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        interpolationParametersSizer.Add((20, 40), 0, 0, 0)

        label_6 = wx.StaticText(self.interpolationParameters, wx.ID_ANY, "Number of Harmonics")
        interpolationParametersSizer.Add(label_6, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        interpolationParametersSizer.Add((20, 10), 0, 0, 0)

        self.numberOfHarmonics = wx.SpinCtrl(self.interpolationParameters,
                                             wx.ID_ANY, "1", min=1, max=100)
        self.numberOfHarmonics.SetToolTip(
            "The number of harmonics used in harmonic decomposition of the pressure and flow waves.")
        interpolationParametersSizer.Add(self.numberOfHarmonics, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.initialEstimates = wx.Panel(self.notebook, wx.ID_ANY)
        self.notebook.AddPage(self.initialEstimates, "Expected ranges")

        initialEstimatesSizer = wx.BoxSizer(wx.VERTICAL)

        label_7 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Compliance", style=wx.ALIGN_CENTER_HORIZONTAL)
        initialEstimatesSizer.Add(label_7, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        initialEstimatesSizer.Add((20, 10), 0, 0, 0)

        complianceGrid = wx.GridSizer(2, 2, 3, 22)
        initialEstimatesSizer.Add(complianceGrid, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        label_8 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Min")
        complianceGrid.Add(label_8, 0, 0, 0)

        label_9 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Max")
        complianceGrid.Add(label_9, 0, 0, 0)

        self.cmin = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        self.cmin.Bind(wx.EVT_TEXT, self._validate)
        complianceGrid.Add(self.cmin, 0, 0, 0)

        self.cmax = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        self.cmax.Bind(wx.EVT_TEXT, self._validate)
        complianceGrid.Add(self.cmax, 0, 0, 0)

        initialEstimatesSizer.Add((20, 10), 0, 0, 0)

        label_12 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Viscoelasticity")
        initialEstimatesSizer.Add(label_12, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        initialEstimatesSizer.Add((20, 10), 0, 0, 0)

        viscoelasticityGrid = wx.GridSizer(2, 2, 3, 22)
        initialEstimatesSizer.Add(viscoelasticityGrid, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        label_10 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Min")
        viscoelasticityGrid.Add(label_10, 0, 0, 0)

        label_11 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Max")
        viscoelasticityGrid.Add(label_11, 0, 0, 0)

        self.kmin = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        self.kmin.Bind(wx.EVT_TEXT, self._validate)
        viscoelasticityGrid.Add(self.kmin, 0, 0, 0)

        self.kmax = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        self.kmax.Bind(wx.EVT_TEXT, self._validate)
        viscoelasticityGrid.Add(self.kmax, 0, 0, 0)

        initialEstimatesSizer.Add((20, 10), 0, 0, 0)

        label_13 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Inertance")
        initialEstimatesSizer.Add(label_13, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        initialEstimatesSizer.Add((20, 10), 0, 0, 0)

        intertanceGrid = wx.GridSizer(2, 2, 3, 22)
        initialEstimatesSizer.Add(intertanceGrid, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        label_14 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Min")
        intertanceGrid.Add(label_14, 0, 0, 0)

        label_15 = wx.StaticText(self.initialEstimates, wx.ID_ANY, "Max")
        intertanceGrid.Add(label_15, 0, 0, 0)

        self.lmin = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        intertanceGrid.Add(self.lmin, 0, 0, 0)

        self.lmax = wx.TextCtrl(self.initialEstimates, wx.ID_ANY, "", validator=_FloatingValidator())
        intertanceGrid.Add(self.lmax, 0, 0, 0)

        self.iterationParameters = wx.Panel(self.notebook, wx.ID_ANY)
        self.notebook.AddPage(self.iterationParameters, "Iteration Parameters")

        iterationParametersSizer = wx.BoxSizer(wx.VERTICAL)

        label_1 = wx.StaticText(self.iterationParameters, wx.ID_ANY, "Number of iterations")
        iterationParametersSizer.Add(label_1, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        iterationParametersSizer.Add((20, 10), 0, 0, 0)

        self.numberOfIterations = wx.TextCtrl(self.iterationParameters, wx.ID_ANY, "")
        self.numberOfIterations.SetToolTip(
            "How many iterations you wish to use to presearch for the fitting." +
            " The higher the value, the higher the wait time, " +
            "but also increases the chances to find a good fit.")
        iterationParametersSizer.Add(self.numberOfIterations, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        iterationParametersSizer.Add((20, 10), 0, 0, 0)

        label_19 = wx.StaticText(self.iterationParameters, wx.ID_ANY, "Search size")
        iterationParametersSizer.Add(label_19, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        iterationParametersSizer.Add((20, 10), 0, 0, 0)

        self.searchSize = wx.TextCtrl(self.iterationParameters, wx.ID_ANY, "")
        self.searchSize.SetToolTip("How many possible points can the initial search choose from.")
        iterationParametersSizer.Add(self.searchSize, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        iterationParametersSizer.Add((20, 10), 0, 0, 0)

        label_20 = wx.StaticText(self.iterationParameters, wx.ID_ANY, "Random Seed")
        iterationParametersSizer.Add(label_20, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        iterationParametersSizer.Add((20, 10), 0, 0, 0)

        self.randomSeed = wx.TextCtrl(self.iterationParameters, wx.ID_ANY, "")
        self.randomSeed.SetToolTip("Initial seed for random search." +
                                   " Keep constant if you wish the same results. Each time you use the program.")
        iterationParametersSizer.Add(self.randomSeed, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.plottingProperties = wx.Panel(self.notebook, wx.ID_ANY)
        self.notebook.AddPage(self.plottingProperties, "Plotting Properties")

        plottingParametersSizer = wx.BoxSizer(wx.VERTICAL)

        plottingColumnsSizer = wx.BoxSizer(wx.HORIZONTAL)

        label_16 = wx.StaticText(self.plottingProperties, wx.ID_ANY, "Significative Digits",
                                 style=wx.ALIGN_CENTER_HORIZONTAL)
        plottingParametersSizer.Add(label_16, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        self.significativeDigits = wx.SpinCtrl(self.plottingProperties, wx.ID_ANY, "1", min=0, max=10)
        self.significativeDigits.SetToolTip("How many digits you wish to display for the searched RCKL values.")
        plottingParametersSizer.Add(self.significativeDigits, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        self.significativeDigits.Bind(wx.EVT_SPINCTRL, self._refreshPlot)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        label_17 = wx.StaticText(self.plottingProperties, wx.ID_ANY, "Font Size")
        plottingParametersSizer.Add(label_17, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        self.fontSize = wx.SpinCtrl(self.plottingProperties, wx.ID_ANY, "4", min=4, max=24)
        self.fontSize.SetToolTip("Font size of the plot labels")
        self.fontSize.Bind(wx.EVT_SPINCTRL, self._refreshPlot)
        plottingParametersSizer.Add(self.fontSize, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        label_18 = wx.StaticText(self.plottingProperties, wx.ID_ANY, "Color")
        plottingParametersSizer.Add(label_18, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        self.color = wx.ColourPickerCtrl(self.plottingProperties, wx.ID_ANY)
        self.color.Bind(wx.EVT_COLOURPICKER_CHANGED,self._refreshPlot)
        plottingParametersSizer.Add(self.color, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        label_21 = wx.StaticText(self.plottingProperties, wx.ID_ANY, "Transparency")
        plottingParametersSizer.Add((20, 10), 0, 0, 0)
        plottingParametersSizer.Add(label_21, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        plottingParametersSizer.Add((20, 10), 0, 0, 0)
        self.alpha = wx.Slider(self.plottingProperties, wx.ID_ANY, 0, 0, 100, style=wx.SL_VALUE_LABEL)
        self.alpha.SetMinSize((200, 32))
        self.alpha.Bind(wx.EVT_SLIDER,self._refreshPlot)
        plottingParametersSizer.Add(self.alpha, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingParametersSizer.Add((20, 10), 0, 0, 0)

        label_22 = wx.StaticText(self.plottingProperties, wx.ID_ANY, "Preview")
        plottingParametersSizer.Add(label_22, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        plottingColumnsSizer.Add(plottingParametersSizer, 1, 0, 0)

        self.preview = wx.Panel(self.plottingProperties, wx.ID_ANY)
        self.preview.SetMinSize((200, 400))
        self.previewSizer = wx.BoxSizer(wx.VERTICAL)
        plottingColumnsSizer.Add(self.preview, 1, 0, 0)


        sizer_2 = wx.StdDialogButtonSizer()
        sizer_1.Add(sizer_2, 0, wx.ALIGN_RIGHT | wx.ALL, 4)

        self.button_OK = wx.Button(self, wx.ID_OK, "")
        self.button_OK.SetDefault()
        sizer_2.AddButton(self.button_OK)

        self.button_CANCEL = wx.Button(self, wx.ID_CANCEL, "")
        sizer_2.AddButton(self.button_CANCEL)

        sizer_2.Realize()

        self.plottingProperties.SetSizer(plottingColumnsSizer)

        self.iterationParameters.SetSizer(iterationParametersSizer)

        self.initialEstimates.SetSizer(initialEstimatesSizer)

        self.interpolationParameters.SetSizer(interpolationParametersSizer)

        self.collectionParameters.SetSizer(collectionParametersSizer)

        self.SetSizer(sizer_1)
        sizer_1.Fit(self)

        self.SetAffirmativeId(self.button_OK.GetId())
        self.SetEscapeId(self.button_CANCEL.GetId())
        self._initializeValues()
        self.Layout()
        self._refreshPlot(None)

    def _initializeValues(self):
        config, code = Config.initialize()
        if code == Config.ReturnCodes.PARSING_ERROR:
            dlg = wx.MessageDialog(self, "Warning error in the configuration file." +
                                   "reloading the default.", "warning", wx.OK | wx.ICON_WARNING)
            dlg.ShowModal()
            dlg.Detroy()
        self.SamplingRate.SetValue(str(config.getVar(
            Config.DataCollectionParameters.SAMPLING_RATE)))
        self.alignment.SetSelection(config.getVar(
            Config.DataCollectionParameters.ALIGNMENT))
        percentage: int = int(config.getVar(Config.DataCollectionParameters.THRESHOLD_FACTOR) * 100)
        self.threshold.SetValue(percentage)

        self.interpolationPoints.SetValue(
            str(config.getVar(Config.InterpolationParameters.INTERPOLATED_RATE)))
        self.interpolationType.SetSelection(
            config.getVar(Config.InterpolationParameters.INTERPOLATION_TYPE))
        self.numberOfHarmonics.SetValue(
            config.getVar(Config.InterpolationParameters.HARMONIC_NUMBER))

        self.cmin.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_C_MIN)))
        self.cmax.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_C_MAX)))
        self.kmin.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_K_MIN)))
        self.kmax.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_K_MAX)))
        self.lmin.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_L_MIN)))
        self.lmax.SetValue(str(config.getVar(Config.IterationParameters.EXPECTED_L_MAX)))

        self.numberOfIterations.SetValue(
            str(config.getVar(Config.IterationParameters.ITERATIONS_NUMBER)))
        self.searchSize.SetValue(
            str(config.getVar(Config.IterationParameters.SEARCH_SIZE)))
        self.randomSeed.SetValue(
            str(config.getVar(Config.IterationParameters.RANDOM_SEED)))
        self.significativeDigits.SetValue(
            str(config.getVar(Config.PlotingProperties.SIGNIFICATIVE_DIGITS)))
        self.fontSize.SetValue(
            str(config.getVar(Config.PlotingProperties.FONTSIZE)))
        alpha = config.getVar(Config.PlotingProperties.ALPHA)
        self.alpha.SetValue(int(alpha * 100))
        R, G, B = config.getVar(Config.PlotingProperties.BOX_COLOR)
        color = wx.Colour(R, G, B)
        self.color.SetColour(color)
        self.button_OK.Enable()

    def _refreshPlot(self, event):
        completeColor = self.color.GetColour()
        color = (completeColor.GetRed() / 255.0,
                 completeColor.GetGreen() / 255.0,
                 completeColor.GetBlue() / 255.0)
        alpha = self.alpha.GetValue() / 100.0
        boxProp = {'boxstyle':'round',
                    'facecolor': color,
                    'alpha': alpha
                    }
        if self._fig is not None:
            self._fig.clf()
        self._fig = Figure((4.8, 4.8), dpi=80)
        fontSize = self.fontSize.GetValue()
        ax = self._fig.add_subplot()
        X = linspace(0, 3.14, 100)
        ax.plot(X, sin(X), 'b', label='Flow simulated', linestyle='dashed')
        ax.plot(X, 0.90 * sin(X) , 'r', label='Flow interpolated')
        ax.legend()
        ax.title.set_text("Preview")
        significativeDigits = self.significativeDigits.GetValue()
        axText = "R = " + "1.123456789"[:significativeDigits + 1] + "\n" + "C = " + "0.3" + "\n" + "K = 0.14" + "\n" + "L = 0.002"
        ax.text(0.75, 0.95, axText, transform=ax.transAxes, fontsize=fontSize,
                verticalalignment='top', bbox=boxProp)
        canvas = FigureCanvas(self.preview, 1, self._fig)
        self.previewSizer.Clear()
        self.previewSizer.Add(canvas, 1, 0, 0)
        self.Layout()
        if event is not None:
            event.Skip()

    def _validate(self, event):
        self.button_OK.Enable(self.Validate())
        self.Layout()
        event.Skip()
