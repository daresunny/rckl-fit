"""
Module for initializing the RCKL computation and then showing various results in a panel.
"""

import os
from typing import Final
from threading import Thread
from threading import Condition
from enum import Enum, auto

from matplotlib import use as pltBackend
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas

from numpy.typing import NDArray
from numpy import float32

import wx
import wx.adv


from config import Config
from recalculateCycleDlg import CycleParametersDialog
import rckl

pltBackend('WXAgg')


class _ComputationTypes(Enum):
    INITIALIZATION = auto()
    EXPORT_CSV = auto()
    SAVE_GRAPHS = auto()
    CYCLE_RECALCULATED = auto()
    CYCLES_REFINED = auto()
    FAILED = auto()
    FATAL_ERROR = auto()


class _NotificationTypes(Enum):
    INITIALIZATION = auto()
    REFINEMENT = auto()


class _PanelWithThreads(wx.Panel):
    def __init__(self, parent: wx.Window):
        super().__init__(parent=parent)
        self.threadRunning = False
        self.exitRequested = False


class _CustomEvent(wx.PyEvent):
    _evttype = None

    @classmethod
    def _getEventType(cls):
        if cls._evttype is None:
            cls._evttype = wx.NewEventType()
        return cls._evttype

    def __init__(self):
        super().__init__()
        self.SetEventType(self._getEventType())

    @classmethod
    def getBinder(cls):
        """Gets the binder for the custom event that you can use
        to bind the UI elements to it."""
        return wx.PyEventBinder(cls._getEventType())


class _UpdateEvent(_CustomEvent):
    def __init__(self, ticks, maxTicks, notificationType: _NotificationTypes):
        super().__init__()
        self.ticks = ticks
        self.maxTicks = maxTicks
        self.notificationType = notificationType


class _DoneEvent(_CustomEvent):
    def __init__(self, eventType: _ComputationTypes):
        super().__init__()
        self.eventType = eventType


class _PlotPeaksEvent(_CustomEvent):
    def __init__(self, condition: Condition, ECG: list[float], peaks: list[int]):
        super().__init__()
        self.condition = condition
        self.ECG = ECG
        self.peaks = peaks


class _PlotAlignementEvent(_CustomEvent):
    def __init__(self, condition, out: list[NDArray[float32],NDArray[float32]]):
        super().__init__()
        self.condition = condition
        self.out = out


class _PlotEvent(_CustomEvent):
    def __init__(self, figure: Figure, selfManaged: bool):
        super().__init__()
        self.figure = figure
        self.selfManaged = selfManaged


class _ExceptionEvent(_CustomEvent):
    def __init__(self, message: str):
        super().__init__()
        self.message = message


class _CriticalErrorEvent(_CustomEvent):
    def __init__(self, message: str):
        super().__init__()
        self.message = message


class _VisualNotifier(rckl.Notifier):
    def __init__(self, notifyWindow: wx.Panel, notificationType: _NotificationTypes):
        self._notifyWindow = notifyWindow
        self._notificationType = notificationType

    def notifyUpdate(self, ticks, maxTicks):
        wx.PostEvent(self._notifyWindow, _UpdateEvent(ticks, maxTicks, self._notificationType))


class _wx_VisualBackend(rckl.VisualBackend):
    def __init__(self, notifyWindow: wx.Panel):
        super().__init__()
        self._notifyWindow = notifyWindow

    def _plotPeaks(self, ECG: list[float], peaks: list[int]):
        condition = Condition()
        condition.acquire()
        wx.PostEvent(self._notifyWindow, _PlotPeaksEvent(condition, ECG, peaks))
        condition.wait()

    def _drawPlot(self, figure, selfManaged: bool):
        wx.PostEvent(self._notifyWindow, _PlotEvent(figure, selfManaged))

    def handleException(self, message):
        wx.PostEvent(self._notifyWindow, _ExceptionEvent(message))

    def alignChannels(self, fun1: NDArray[float32], fun2: NDArray[float32]) ->\
            tuple[NDArray[float32], NDArray[float32]]:
        condition = Condition()
        condition.acquire()
        out = [fun1, fun2]
        wx.PostEvent(self._notifyWindow, _PlotAlignementEvent(condition, out))
        condition.wait()
        return (out[0], out[1])



class _CalculationThread(Thread):
    def __init__(self, notifyWindow: _PanelWithThreads):
        super().__init__()
        notifyWindow.threadRunning = True


class _DataCalculateThread(_CalculationThread):
    def __init__(self, notifyWindow: wx.Panel, datapath: str, config: Config, order: tuple[int, int, int],
                 progressUpdater: rckl.ProgressUpdate, plotBackend: rckl.VisualBackend):
        super().__init__(notifyWindow)
        self._notifyWindow = notifyWindow
        self._datapath = datapath
        self._config = config
        self._order = order
        self._progressUpdater = progressUpdater
        self._plotBackend = plotBackend
        self._data = None
        self._running = True

    def run(self):
        try:
            self._data = rckl.RCKL_Computation(self._datapath, self._config, self._order,
                                               self._progressUpdater, self._plotBackend)
        except rckl.DataLoadingException as exception:
            self._notifyWindow.threadRunning = False
            wx.PostEvent(self._notifyWindow, _CriticalErrorEvent(exception.message))
            return
        finally:
            self._running = False
        wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.INITIALIZATION))

    def getData(self) -> rckl.RCKL_Computation:
        """Gets the rckl data once it is computed."""
        return self._data


class _DataRefineThread(_CalculationThread):
    def __init__(self, notifyWindow: wx.Panel, data: rckl.RCKL_Computation):
        super().__init__(notifyWindow)
        self._data = data
        self._notifyWindow = notifyWindow

    def run(self):
        self._data.refine()
        wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.CYCLES_REFINED))


class _DataNotifyThread(Thread):
    def __init__(self, updater: rckl.ProgressUpdate, notifier: _VisualNotifier):
        super().__init__()
        self._updater = updater
        self._notifier = notifier

    def run(self):
        self._updater.subscribe(self._notifier)


class _DataSaveThread(_CalculationThread):
    def __init__(self, notifyWindow: wx.Panel, data: rckl.RCKL_Computation, filename: str):
        super().__init__(notifyWindow)
        self._data = data
        self._filename = filename
        self._notifyWindow = notifyWindow

    def run(self):
        if self._data.exportCSV(self._filename):
            wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.EXPORT_CSV))
        else:
            wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.FAILED))


class _GraphSaveThread(_CalculationThread):
    def __init__(self, notifyWindow: wx.Panel, data: rckl.RCKL_Computation, filename: str):
        super().__init__(notifyWindow)
        self._data = data
        self._filename = filename
        self._notifyWindow = notifyWindow

    def run(self):
        if self._data.saveGraphs(self._filename):
            wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.SAVE_GRAPHS))
        else:
            wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.FAILED))


class _RecalculateCycleThread(_CalculationThread):
    def __init__(self, notifyWindow: wx.Panel, data: rckl.RCKL_Computation, cycle: int):
        super().__init__(notifyWindow)
        self._data = data
        self._cycle = cycle
        self._notifyWindow = notifyWindow
        self.Cmin: float
        self.Cmax: float
        self.Cval: float
        self.Kmin: float
        self.Kmax: float
        self.Kval: float
        self.Lmin: float
        self.Lmax: float
        self.Lval: float

    def run(self):
        L = [(self.Cmin, self.Cmax), (self.Kmin, self.Kmax), (self.Lmin, self.Lmax)]
        self._data.recalculateCycle(self._cycle,
                                    self.Cval, self.Kval, self.Lval,
                                    L, False)
        wx.PostEvent(self._notifyWindow, _DoneEvent(_ComputationTypes.CYCLE_RECALCULATED))


class _DrawCycleThread(Thread):
    def __init__(self, data: rckl.RCKL_Computation, cycle: int, selfManaged: bool):
        super().__init__()
        self._cycle = cycle
        self._data = data
        self._selfManaged = selfManaged

    def run(self):
        self._data.drawCycle(self._cycle, self._selfManaged)


class _DrawParametersThread(Thread):
    def __init__(self, notifyWindow, data: rckl.RCKL_Computation, variable: rckl.FittingVariables, selfManaged: bool):
        super().__init__()
        self._data = data
        self._variable = variable
        self._selfManaged = selfManaged
        self._notifyWindow = notifyWindow

    def run(self):
        self._data.drawParameters(self._variable, self._selfManaged)


class AnalysisPanel(_PanelWithThreads):
    """Panel which shows the various plots once the RCKL data has been computed."""
    _etc: Final[str] = "...."

    def __init__(self, parent: wx.Frame, path: str):
        super().__init__(parent)

        self._updateCounter = 0
        self._path = path
        self._numberOfCycles = 0
        self._currentCycle = 1

        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        self._hideables: list[wx.Window] = []
        self._rcklData: rckl.RCKL_Computation = None
        self._progressUpdater: rckl.ProgressUpdate = rckl.ProgressUpdate(1)
        self._loadSizer: wx.BoxSizer = None

        self.title = wx.StaticText(self, wx.ID_ANY, "")
        sizer_1.Add(self.title, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)
        sizer_1.Add((20, 60), 0, 0, 0)

        self.drawTitle = wx.StaticText(self, wx.ID_ANY, "")
        sizer_1.Add(self.drawTitle, 0, wx.LEFT, 10)

        self.drawChoice = wx.Choice(self, wx.ID_ANY,
                                    choices=["Cycles", "Resistance", "Compliance", "Viscoelasticity", "Inertance"])
        self.drawChoice.SetSelection(0)
        self.drawChoice.Bind(wx.EVT_CHOICE, self._switchPlot)
        self._hideables.append(self.drawChoice)
        sizer_1.Add(self.drawChoice, 0, wx.LEFT | wx.TOP, 4)

        self._graphLabel = wx.StaticText(self, wx.ID_ANY, "Cycle")
        sizer_1.Add(self._graphLabel, 0, wx.ALIGN_CENTER_HORIZONTAL, 0)

        cyclesSizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(cyclesSizer, 0, wx.EXPAND, 0)

        cyclesSizer.Add((40, 0), 0, 0, 0)

        self.leftArrow = wx.Button(self, wx.ID_ANY, "<---")
        cyclesSizer.Add(self.leftArrow, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        cyclesSizer.Add((40, 0), 0, 0, 0)
        self._hideables.append(self.leftArrow)
        self.leftArrow.Bind(wx.EVT_BUTTON, self._switchCycle)

        self.cyclePanel = wx.Panel(self, wx.ID_ANY, size=wx.Size(350, 350))
        self._hideables.append(self.cyclePanel)
        parent.SetMinSize(wx.Size(840, 600))
        cyclesSizer.Add(self.cyclePanel, 1, 0, 0)
        cyclesSizer.Add((40, 0), 0, 0, 0)
        self.cyclePanelContainer = wx.BoxSizer(wx.VERTICAL)
        self.cyclePanel.SetSizer(self.cyclePanelContainer)
        self.rightArrow = wx.Button(self, wx.ID_ANY, "--->")
        self.rightArrow.Bind(wx.EVT_BUTTON, self._switchCycle)
        cyclesSizer.Add(self.rightArrow, 0, wx.ALIGN_CENTER_VERTICAL, 0)
        self._hideables.append(self.rightArrow)

        cyclesSizer.Add((50, 0), 0, 0, 0)

        sizer_1.Add((0, 40), 0, 0, 0)

        controlButtonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        sizer_1.Add(controlButtonsSizer, 1, wx.ALIGN_CENTER_HORIZONTAL, 0)

        self.exportValues = wx.Button(self, wx.ID_ANY, "Export values as a CSV")
        controlButtonsSizer.Add(self.exportValues, 0, 0, 0)
        self._hideables.append(self.exportValues)

        controlButtonsSizer.Add((20, 0), 0, 0, 0)

        self.exportCycles = wx.Button(self, wx.ID_ANY, "Export cycles as a pdf")
        controlButtonsSizer.Add(self.exportCycles, 0, 0, 0)
        self._hideables.append(self.exportCycles)

        controlButtonsSizer.Add((20, 0), 0, 0, 0)

        self.recalculateCycles = wx.Button(self, wx.ID_ANY, "Recalculate Cycle")
        controlButtonsSizer.Add(self.recalculateCycles, 0, 0, 0)
        self._hideables.append(self.recalculateCycles)

        controlButtonsSizer.Add((20, 0), 0, 0, 0)

        self.refineCalculations = wx.Button(self, wx.ID_ANY, "Refine Calculations")
        controlButtonsSizer.Add(self.refineCalculations)
        self._hideables.append(self.refineCalculations)

        self.SetSizer(sizer_1)
        self._toggleHideables(False)

        self.Bind(_UpdateEvent.getBinder(), handler=self._updateProgress)
        self.Bind(_DoneEvent.getBinder(), handler=self._finishComputation)
        self.Bind(_PlotPeaksEvent.getBinder(), handler=self._plotPeaks)
        self.Bind(_PlotEvent.getBinder(), handler=self._plot)
        self.Bind(_ExceptionEvent.getBinder(), self._handleException)
        self.Bind(_CriticalErrorEvent.getBinder(), self._onCriticalError)
        self.Bind(_PlotAlignementEvent.getBinder(), self._realignChannels)
        parent.Bind(wx.EVT_CLOSE, self._onExitRequested)
        self.exportValues.Bind(wx.EVT_BUTTON, self._exportCSV)
        self.exportCycles.Bind(wx.EVT_BUTTON, self._saveGraphs)
        self.recalculateCycles.Bind(wx.EVT_BUTTON, self._recalculateCycle)
        self.refineCalculations.Bind(wx.EVT_BUTTON, self._refineCycles)

        self._visualBackend = _wx_VisualBackend(self)
        self._dataCalculateThread: _DataCalculateThread = None
        self._dataRefineThread: _DataRefineThread = None

    def loadData(self, order: tuple[int, int, int]):
        """Once the UI elements of the panel have been initialized,
        call this method compute and load the rckl data."""
        self.title.SetLabel("Calculating for the file: " + os.path.basename(self._path))
        config, returnCode = Config.initialize()
        match returnCode:
            case Config.ReturnCodes.PARSING_ERROR:
                wx.MessageBox("Unable to read the configuration file syntax, reloading the default config.")
            case Config.ReturnCodes.CONFIG_NOT_FOUND:
                wx.MessageBox("Unable to find the configuration file. Loading a default one.")
        self._dataCalculateThread = _DataCalculateThread(self, self._path,
                                                         config, order, self._progressUpdater, self._visualBackend)
        notifier = _VisualNotifier(self, _NotificationTypes.INITIALIZATION)
        updateThread = _DataNotifyThread(self._progressUpdater, notifier)
        self._dataCalculateThread.start()
        updateThread.start()
        self.Layout()

    def _plotPeaks(self, event: _PlotPeaksEvent):
        condition = event.condition
        ECG = event.ECG
        peaks = event.peaks
        rckl.VisualBackend.initializePeaksPlot(ECG, peaks)
        plt.pause(-1)
        condition.acquire()
        condition.notify_all()
        condition.release()

    def _realignChannels(self, event: _PlotAlignementEvent):
        out = event.out
        condition = event.condition
        rckl.VisualBackend.initializeAlignementPlot(out)
        plt.pause(-1)
        condition.acquire()
        condition.notify_all()
        condition.release()

    def _plot(self, event: _PlotEvent):
        figure = event.figure
        selfManaged = event.selfManaged
        if selfManaged:
            figure.show()
        else:
            self.cyclePanelContainer.Clear(True)
            self.cyclePanel.SetMinSize(wx.Size(80 * self._visualBackend.dimension,
                                               80 * self._visualBackend.dimension))
            canvas = FigureCanvas(self.cyclePanel, 1, figure)
            canvas.Bind(wx.EVT_LEFT_DCLICK, self._onDoubleClickEvent)
            self.cyclePanelContainer.Add(canvas)
            self.GetParent().Layout()

    def _updateProgress(self, event: _UpdateEvent):
        ticks = event.ticks
        maxTicks = event.maxTicks
        notificationType = event.notificationType
        self._updateCounter = (self._updateCounter + 1) % 4
        label: str
        match notificationType:
            case _NotificationTypes.INITIALIZATION:
                label = "Computing cycle: " + str(ticks + 1) + " out of " + str(maxTicks)
            case _NotificationTypes.REFINEMENT:
                if ticks < maxTicks / 2:
                    label = "Part 1 out of 2:\nComputing cycle "\
                        + str(ticks + 1) + " out of " + str(maxTicks // 2)
                if ticks >= maxTicks / 2:
                    label = "Part 2 out of 2:\nComputing cycle "\
                        + str(ticks - maxTicks // 2 + 1) + " out of " + str(maxTicks // 2)
        self._graphLabel.SetLabel(label + " " + AnalysisPanel._etc[:self._updateCounter])
        self.GetParent().Layout()

    def _finishComputation(self, event: _DoneEvent):
        eventType = event.eventType
        self.threadRunning = False
        if self.exitRequested:
            self.GetParent().Close()
            return
        self._toggleHideables(True)
        self._graphLabel.SetLabel("")
        match eventType:
            case _ComputationTypes.INITIALIZATION:
                self._rcklData = self._dataCalculateThread.getData()
                self._numberOfCycles = self._rcklData.getNumberOfCycles()
                self.Bind(wx.EVT_CHAR_HOOK, self._onKeyEvent)
                thread = _DrawCycleThread(self._rcklData, 1, selfManaged=False)
                self.Layout()
                thread.start()

            case _ComputationTypes.EXPORT_CSV:
                wx.MessageBox("The values were exported successfully", "Success", style=wx.OK)

            case _ComputationTypes.SAVE_GRAPHS:
                wx.MessageBox("The graphs of the cycles were saved successfully", "Success", style=wx.OK)

            case _ComputationTypes.CYCLE_RECALCULATED:
                self._redrawPlot(False)

            case _ComputationTypes.CYCLES_REFINED:
                self._redrawPlot(False)

            case _ComputationTypes.FAILED:
                pass

    def _toggleHideables(self, value: bool):
        if value:
            for x in self._hideables:
                x.Show(value)
        else:
            for x in self._hideables:
                x.Hide()

    def _redrawPlot(self, selfManaged: bool):
        selection = self.drawChoice.GetSelection()
        if selection == 0:
            thread = _DrawCycleThread(self._rcklData, self._currentCycle, selfManaged)
            thread.start()
            return
        thread = _DrawParametersThread(self, self._rcklData, selection - 1, selfManaged)
        self.GetParent().Layout()
        thread.start()

    def _switchCycle(self, event: wx.CommandEvent):
        button = event.GetEventObject()
        if button == self.leftArrow:
            self._currentCycle -= 1
        elif button == self.rightArrow:
            self._currentCycle += 1
        if self._currentCycle == 0:
            self._currentCycle = self._numberOfCycles
        elif self._currentCycle == self._numberOfCycles + 1:
            self._currentCycle = 1
        self.GetParent().Layout()
        self._redrawPlot(False)

    def _switchPlot(self, _event):
        if self.drawChoice.GetSelection() == 0:
            self.leftArrow.Enable(True)
            self.rightArrow.Enable(True)
        else:
            self.leftArrow.Enable(False)
            self.rightArrow.Enable(False)

        self._redrawPlot(False)
        self.GetParent().Layout()

    def _exportCSV(self, _event):
        saveFileDialog = wx.FileDialog(self, "Export the values.", "", "", "*.csv", wx.FD_SAVE)
        result = saveFileDialog.ShowModal()
        if result == wx.ID_OK:

            path = saveFileDialog.GetPath()
            self._toggleHideables(False)
            self._graphLabel.SetLabel("Exporting data into a csv file...")
            thread = _DataSaveThread(self, self._rcklData, path)
            thread.start()

    def _saveGraphs(self, _event):
        saveFileDialog = wx.FileDialog(self, "Save the graphs", "", "", "*.pdf", wx.FD_SAVE)
        result = saveFileDialog.ShowModal()
        if result == wx.ID_OK:
            path = saveFileDialog.GetPath()
            self._toggleHideables(False)
            self._graphLabel.SetLabel("Saving the graphs...")
            thread = _GraphSaveThread(self, self._rcklData, path)
            thread.start()

    def _recalculateCycle(self, _event):
        cycle = self._currentCycle
        array = self._rcklData.results()
        C = array[cycle - 1, 1]
        K = array[cycle - 1, 2]
        L = array[cycle - 1, 3]
        dialog = CycleParametersDialog((C, K, L), self)
        if dialog.ShowModal() == wx.ID_OK:
            thread = _RecalculateCycleThread(self, self._rcklData, cycle)
            thread.Cmin = float(dialog.Cmin.GetValue())
            thread.Cmax = float(dialog.Cmax.GetValue())
            thread.Cval = float(dialog.Cval.GetValue())
            thread.Kmin = float(dialog.Kmin.GetValue())
            thread.Kmax = float(dialog.Kmax.GetValue())
            thread.Kval = float(dialog.Kval.GetValue())
            thread.Lmin = float(dialog.Lmin.GetValue())
            thread.Lmax = float(dialog.Lmax.GetValue())
            thread.Lval = float(dialog.Lval.GetValue())
            self._graphLabel.SetLabel("Recomputing cycle ...")
            self._toggleHideables(False)
            thread.start()

    def _refineCycles(self, _event):
        notifier = _VisualNotifier(self, _NotificationTypes.REFINEMENT)
        self._dataRefineThread = _DataRefineThread(self, self._rcklData)
        notificationThread = _DataNotifyThread(self._progressUpdater, notifier)
        self._toggleHideables(False)
        self._dataRefineThread.start()
        notificationThread.start()

    def _handleException(self, event: _ExceptionEvent):
        message = event.message
        wx.MessageBox(message, "An error occured!", wx.OK | wx.ICON_WARNING)

    def _onKeyEvent(self, event: wx.KeyEvent):
        code = event.GetKeyCode()
        match code:
            case wx.WXK_NUMPAD_ADD:
                self._visualBackend.dimension += 1
                self._redrawPlot(False)
            case wx.WXK_NUMPAD_SUBTRACT:
                self._visualBackend.dimension -= 1
                self._redrawPlot(False)

    def _onDoubleClickEvent(self, _event: wx.MouseEvent):
        self._redrawPlot(True)

    def _onCriticalError(self, event: _CriticalErrorEvent):
        message = event.message
        self.title.SetLabel("")
        self._graphLabel.SetLabel("")
        self.GetParent().Layout()
        wx.MessageBox(message, "Critical Error!", wx.OK | wx.ICON_ERROR)
        self.GetParent().Close()

    def _onExitRequested(self, event: wx.CloseEvent):
        if not event.CanVeto() or not self.threadRunning:
            event.Skip()
            return
        event.Veto()
        if self.exitRequested:
            return
        self.exitRequested = True
        plt.close('all')
        self.title.SetLabel("Exiting. Please wait until the program finishes.")
        self.GetParent().Layout()
        self._progressUpdater.requestStop()
