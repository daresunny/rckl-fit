'''
Constants used in the RCKL program
'''
from enum import Enum
from enum import StrEnum
from enum import auto
from enum import unique
from typing import Final
from typing import Self
from typing import Optional
from pathlib import Path
import os
import platform
import toml
import numpy as np


@unique
class InterpolationType(Enum):
    """Class for choosing how to interpolate the data."""
    SPLINE = 0
    FOURIER = 1


@unique
class Alignment(Enum):
    """Choose whether you want to align data
    automatically, maunually or note at all."""
    NONE = 0
    AUTO = 1
    MANUAL = 2


class Config:
    """Class enabling the loading of configuration from a TOML file."""

    if platform.system() == "Linux":
        __CONFIG_FILE_LOCATION: Final[str] = os.path.expanduser("~/.config/RCKL")
        __CONFIG_FILE_NAME: Final[str] = __CONFIG_FILE_LOCATION + "/config.toml"
    if platform.system() == "Windows":
        __CONFIG_FILE_LOCATION: Final[str] = os.getenv("APPDATA")
        __CONFIG_FILE_NAME: Final[str] = __CONFIG_FILE_LOCATION + "\\config.toml"
    Path(__CONFIG_FILE_LOCATION).mkdir(parents=True, exist_ok=True)

    class Defaults:
        """Default values for the parameters"""
        # * Data collection parameters
        SAMPLING_RATE: Final[int] = 1000
        ALIGNMENT: Alignment = Alignment.AUTO.value
        THRESHOLD_FACTOR: Final[np.float32] = np.float32(0.93)

        # * Interpolation parameters

        INTERPOLATED_RATE: Final[int] = 100
        INTERPOLATION_TYPE: Final[InterpolationType] =\
            InterpolationType.FOURIER.value
        HARMONIC_NUMBER: Final[int] = 10

        # * Iterations parameters to find the local minimum
        EXPECTED_C_MIN: Final[float] = pow(10, -7)
        EXPECTED_C_MAX: Final[float] = pow(10, -3)
        EXPECTED_K_MIN: Final[float] = 0.3
        EXPECTED_K_MAX: Final[float] = 1.3
        EXPECTED_L_MIN: Final[float] = pow(10, -6)
        EXPECTED_L_MAX: Final[float] = pow(10, -4)

        ITERATIONS_NUMBER: Final[int] = 500
        SEARCH_SIZE: Final[int] = 5000
        RANDOM_SEED: Final[int] = 2718281

        # * Plotting properties:
        SIGNIFICATIVE_DIGITS: Final[int] = 4
        FONTSIZE: Final[int] = 12
        BOX_COLOR: Final[tuple[int, int, int]] = (0, 188, 227)
        ALPHA: Final[float] = 0.5

    @unique
    class ReturnCodes(Enum):
        """Return codes during initialization of the config file"""
        SUCCESS = auto()
        PARSING_ERROR = auto()
        CONFIG_NOT_FOUND = auto()
        UNABLE_TO_SAVE = auto()

    class __Parameters(Enum):
        pass

    class DataCollectionParameters(__Parameters):
        """Parameters mentioning how data is collected.

        SAMPLING RATE: Data sampling rate in Hz.
        ALIGNEMENT: Choosing whether to align flow and pressure by hand or
                    automatically by searching for a minimum or not at all.
        """
        SAMPLING_RATE = auto()
        ALIGNMENT = auto()
        THRESHOLD_FACTOR = auto()

    class InterpolationParameters(__Parameters):
        """Parameters determining how the flow curve will be interpolated."""
        INTERPOLATED_RATE = auto()
        INTERPOLATION_TYPE = auto()
        HARMONIC_NUMBER = auto()

    class IterationParameters(__Parameters):
        """Parameters determining how the iterations
           to find the optimal fit proceed."""
        EXPECTED_C_MIN = auto()
        EXPECTED_C_MAX = auto()
        EXPECTED_K_MIN = auto()
        EXPECTED_K_MAX = auto()
        EXPECTED_L_MIN = auto()
        EXPECTED_L_MAX = auto()
        ITERATIONS_NUMBER = auto()
        SEARCH_SIZE = auto()
        RANDOM_SEED = auto()

    class PlotingProperties(__Parameters):
        """Properties of the plot on which the flow will be represented."""
        SIGNIFICATIVE_DIGITS = auto()
        FONTSIZE = auto()
        BOX_COLOR = auto()
        ALPHA = auto()

    def __init__(self, d: Optional[dict] = None):
        if d is None:
            self.__dictionary = {}
        else:
            self.__dictionary = d.copy()
        self.__setDefaults()

    @classmethod
    def _RetConfig(cls):
        return cls.ReturnCodes

    @classmethod
    def initialize(cls) -> tuple[Self, ReturnCodes]:
        """Initializes a new configuration from the configuration file."""
        output: Config
        returnCode = cls.ReturnCodes.SUCCESS
        try:
            with open(cls.__CONFIG_FILE_NAME, 'r', encoding='utf-8') as file:
                output = Config(toml.load(file))
        except toml.TomlDecodeError:
            output = Config()
            returnCode = cls.ReturnCodes.PARSING_ERROR
        except FileNotFoundError:
            output = Config()
            returnCode = cls.ReturnCodes.CONFIG_NOT_FOUND
        return output, returnCode

    def getVar(self, name: __Parameters):
        """Get a specified parameter from the configuration."""
        return self.__dictionary.get(name)

    def saveConfig(self):
        """Saves the current configuration into a TOML file"""
        returnCode = self._RetConfig().SUCCESS
        try:
            with open(Config.__CONFIG_FILE_NAME, 'w', encoding='utf-8') as file:
                toml.dump(self.__dictionary, file)
        except IOError:
            returnCode = self._RetConfig().UNABLE_TO_SAVE
        return returnCode

    def __setDefaults(self) -> None:
        d = self.__dictionary
        d.setdefault(self.DataCollectionParameters.ALIGNMENT,
                     self.Defaults.ALIGNMENT)
        d.setdefault(self.DataCollectionParameters.SAMPLING_RATE,
                     self.Defaults.SAMPLING_RATE)
        d.setdefault(self.DataCollectionParameters.THRESHOLD_FACTOR,
                     self.Defaults.THRESHOLD_FACTOR)
        d.setdefault(self.InterpolationParameters.INTERPOLATED_RATE,
                     self.Defaults.INTERPOLATED_RATE)
        d.setdefault(self.InterpolationParameters.INTERPOLATION_TYPE,
                     self.Defaults.INTERPOLATION_TYPE)
        d.setdefault(self.InterpolationParameters.HARMONIC_NUMBER,
                     self.Defaults.HARMONIC_NUMBER)

        d.setdefault(self.IterationParameters.EXPECTED_C_MIN,
                     self.Defaults.EXPECTED_C_MIN)
        d.setdefault(self.IterationParameters.EXPECTED_C_MAX,
                     self.Defaults.EXPECTED_C_MAX)
        d.setdefault(self.IterationParameters.EXPECTED_K_MIN,
                     self.Defaults.EXPECTED_K_MIN)
        d.setdefault(self.IterationParameters.EXPECTED_K_MAX,
                     self.Defaults.EXPECTED_K_MAX)
        d.setdefault(self.IterationParameters.EXPECTED_L_MIN,
                     self.Defaults.EXPECTED_L_MIN)
        d.setdefault(self.IterationParameters.EXPECTED_L_MAX,
                     self.Defaults.EXPECTED_L_MAX)
        d.setdefault(self.IterationParameters.ITERATIONS_NUMBER,
                     self.Defaults.ITERATIONS_NUMBER)
        d.setdefault(self.IterationParameters.SEARCH_SIZE,
                     self.Defaults.SEARCH_SIZE)
        d.setdefault(self.IterationParameters.RANDOM_SEED,
                     self.Defaults.RANDOM_SEED)

        d.setdefault(self.PlotingProperties.SIGNIFICATIVE_DIGITS,
                     self.Defaults.SIGNIFICATIVE_DIGITS)
        d.setdefault(self.PlotingProperties.FONTSIZE,
                     self.Defaults.FONTSIZE)
        d.setdefault(self.PlotingProperties.BOX_COLOR,
                     self.Defaults.BOX_COLOR)
        d.setdefault(self.PlotingProperties.ALPHA,
                     self.Defaults.ALPHA)
