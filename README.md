Windkessel model fitting Python module
=======================================
**Author:** *Michal Cizek*

# Introduction
This module introduces a class called RCKL_Computation, the goal of which is to provide a fit of data a measured curve of blood flow (TCD), provided, pressure (MAP) and electrocardiogram (ECG) data.

The class uses this data to provide the best fit for the simulated TCD curve with the following four parameters:

* Resistance (R) in mmHg/(mL/min)
* Compliance (C) in ml/mmHg
* Viscoelasticity (K) in mmHg/(mL/min)
* Inertance (L) in in mmHg/(mL/min²)

It detects cardiac cycles provided the ECG data (with possibility to add/remove missed/wrong cycles) and then calculates the R, C, K, L values per each cycle.

Once calculated the user can do the following:

* Get the data in form of a numpy array.
* Draw each for each cardiac cycle the corresponding measured and simulated TCD curve.
* Save the drawings of all cardiac cycles into a pdf document.
* Export the RCKL array into a csv file.

To read more on the Windkessel model and how it is used in physiology research, please check the reference section of this readme file.

# Usage
## Data Format
This program expects the input data in the following form:
A matlab file binary with the following parameters:
* datastart: `[[x1,y1,z1]]` Where x1,y1,z1 represent the starting indexes of respectively ECG,MAP and TCD.
* dataend: `[[x2,y2,z2]]` Where x2,y2,z2 represent the ending indexes of respectively ECG, MAP and TCD.
* data `[[m1,....,mn]]` Array representing the data. ECG values are between index x1 and x2, MAP between y1 and y2 and finally TCD between z1 and z2
* samplerate `[[f]]`, where f represents the sampling frequency of the data. This last parameter is optional and if missing can be loaded from the config file.

## Tutorial
The most straightforward way to use this module is to simply get a data file with for example the path "/home/user/data" and call the following code:

``` python
from rckl import RCKL_Computation
r = RCKL_Computation('/home/user/data')
```

Next the program is going to read the ECG file and based on the peaks will show the candidates for cycles. The user then can add or remove peaks by clicking on the individual peaks. The peak detection threshold can be set in the config.py file.

Once this task done, if requested in the config file, the program will attempt to realign the pressure and the flow curve, if the two are out of phase due to for instance being measured in different parts of the body. The realignement can either be ommited, set to manual or automatic as chosen in the config file.

If manual realignment is chosen the program will show each individual cycle and then the curves can be moved by arrows on the keyboard and then validated by pressing the enter key.

Once calculated, you can get the numpy matrix containing the RCKL values per cycle with the following command:

``` python
r.results()
```

An example of an output then will be:

``` python-console
array([[1.36962008e+00, 3.84365951e-04, 3.00035514e-01, 7.63163736e-07],
       [1.32250011e+00, 1.39390347e-04, 1.29818734e+00, 5.32329907e-06],
       [1.32371569e+00, 3.45150888e-04, 1.24573382e+00, 2.42197609e-05],
       [1.34871781e+00, 1.28745442e-04, 1.04027229e+00, 9.99061924e-04],
       [1.33312941e+00, 1.00000384e-07, 1.14565867e+00, 2.34833409e-06],
       [1.29174662e+00, 3.07030523e-04, 8.12559111e-01, 1.08104225e-06],
       [1.25927687e+00, 4.37894524e-04, 3.08503598e-01, 4.08847247e-05],
       [1.24351633e+00, 4.21390451e-04, 3.00066661e-01, 1.19821734e-07],
       [1.17555928e+00, 1.69016176e-04, 9.15161245e-01, 6.79774326e-04],
       [1.14342058e+00, 2.23240993e-04, 9.78498363e-01, 3.53681713e-06],
       [1.13179123e+00, 1.79764254e-04, 8.07553838e-01, 6.28304793e-04],
       [1.15949631e+00, 3.85534251e-04, 3.01969834e-01, 4.48347805e-05],
       [1.19746411e+00, 3.66382812e-04, 3.00672338e-01, 6.11329145e-07],
       [1.22838140e+00, 1.86843757e-04, 1.26215706e+00, 6.78857885e-04],
       [1.23571157e+00, 3.82902997e-04, 3.42604461e-01, 9.31283536e-07],
       [1.22342038e+00, 2.27983896e-04, 7.65372460e-01, 5.00023800e-04],
       [1.22598445e+00, 4.36309754e-04, 3.00073366e-01, 1.77471397e-05],
       [1.21703970e+00, 2.74180468e-04, 6.67475444e-01, 3.52288038e-04],
       [1.19413507e+00, 1.50456250e-04, 6.16185886e-01, 8.20603981e-04],
       [1.19935358e+00, 3.02393320e-04, 1.29905126e+00, 3.93391298e-04],
       [1.19227457e+00, 4.81641107e-04, 3.00024804e-01, 1.00759790e-07],
       [1.17167735e+00, 3.82191054e-04, 5.17222421e-01, 3.34945263e-06],
       [1.12467027e+00, 3.36947520e-04, 3.31973894e-01, 1.31891094e-04],
       [1.12518716e+00, 3.40083605e-04, 3.28119623e-01, 9.35041900e-06],
       [1.12014401e+00, 1.61548288e-04, 5.03074681e-01, 1.49998086e-04],
       [1.15040231e+00, 1.23138884e-05, 1.09703029e+00, 4.94775298e-04],
       [1.21110332e+00, 3.12996066e-04, 3.00978821e-01, 5.64112366e-07],
       [1.28857350e+00, 1.00342583e-04, 4.33262746e-01, 9.87786521e-04]])

```
The rows of the resulting array correspond to the cycles, while the columns are the parameters in the order R, C, K, and L.

You can then plot the individual values to see how they change over time, using the method drawParamaters and the number of the parameter to be plotted in the same order. For example if you wish to plot the evolution of the parameter K, you can put the command:

``` python
r.drawParameters(3)
```

You can then plot for each individual cycle the simulated and measured TCD curves with the method drawCycle. For example if you wish to draw the fifth cycle, you can put the command:

``` python
r.drawCycle(5)
```

You can save the drawings of all cycles into a single pdf file using the method saveGraphs. For example to save the drawings into the file with the path "/home/user/cycles.pdf", you can put the command:

``` python
r.saveGraphs('/home/user/cycles.pdf')
```

Finally you can export the numpy matrix that we obtained earlier into a csv file using the method exportCSV. For example if you want to save it to the file "/home/user/matrix.csv", you can put the command:

``` python
r.exportCSV('/home/user/matrix.csv')
```

The results that you obtain might not always be very accurate, if you wish to improve upon the you have two ways to do so. You can either run 

``` python
r.refine()
```
Which is a method that recalculates RCKL fits based on the value of a previous fit as a starting point.Under the assumption that two consecutive cycles should have relatively close RCKL values, this method should help to avoid escaping some potential local minima.

If after this you are still unsatisfied, you can attempt to recalculate certain cycles by providing your own starting value and possibly new ranges if you so desire. For instance if we wish to recalculate the C,K,L values (note that R is not needed to be fitted since it is just mean pressure over mean flow), with the new initial values 0.02,0.7 and 0.003 and the bounds for C between 0.00001 and 1.2, for K between 0.08 and 7 and L between 0.000009 and 0.008, for the cycle 8 you can put:

``` python
r.recalculateCycle(8, 0.03, 0.7, 0.004, [(0.00001, 1.2), (0.08, 7), (0.000009, 0.008)])
```

Please note that the minimization algorithm used by Python might not always respect the bounds that you put and it is possible that depending on your bounds and starting values, you end up with negative numbers.

## Config file

The configuration of the module is stored in the config.py file it contains customizable parameters for the fits you desire.
Constants that you can change are:

* SAMPLING_RATE: This should be an integer representing how many measurments are taking per second. If this value is already specified in the matlab file, this constant will not be used.

* UNIT_TIME_FACTOR: By default 60, simply represents how many seconds there are in a minute. Unless you need to for some reason change units, this part should not be needed to be modified.

* ALIGNMENT: Choose how you wish to align the TCD and MAP together. The possible values are: Alignment.NONE if the two are already aligned. Alignment.AUTO if you wish the program to automatically align the two minimal values found in the cycle and Alignment.MANUAL if you wish to manually align the two curves.

* THRESHOLD_FACTOR: Choose what percentage of maximum amplitude of the ECG counts for the detection of the peaks. The lower this value is, the higher the chance that no peaks will be missed, but also the higher chance that you will add wrong peaks. At the end you will have the chance to manually remove false positives and add missed peaks.

* INTERPOLATED_RATE: An integer indicating how many regularly spaced points in the cycle should be used. The module then interpolates those points based on the measured TCD curve and uses them for the fit calculation.

* INTERPOLATION_TYPE: Choose how the points are interpolated you can either put the value InterpolationType.FOURIER to interpolate it by Fourier series or InterpolationType.SPLINE to use the cubic spline interpolation method.

* HARMONIC_NUMBER: Integer that represents the amount of harmonics used in the Fourier interpolation as well as in the RCKL fit.

* RCKL_EXPECTED_BOUNDS: An array of the form \[(Cmin,Cmax),(Kmin,Kmax),(Lmin,Lmax)\] representing the expected range of the C,K and L values in the fit.

* ITERATIONS_NUMBER: To find the right fit and avoid the get trapped in a local minimum, the module precalculates the specified number of random values in the given range and picks the best of them as the initial starting point in the fitting problem. This integer determines how many random points are calculated

* SEARCH_SIZE: This integer represents into how many points the range of possible values gets split for the random generation.

* RANDOM_SEED: The seed used for the generation of the pseudorandom points. For the sake of reproducibility of results this seed should remain constant during the experiment.

* SIGNIFICATIVE_DIGITS: How many digits should be displayed in the RCKL values on the plot.

* FONTSIZE: The size of the letters in the plot.

* BOX_COLOR: the color of the box where RCKL are represented on the plot.

* APLHA: Alpha value of the color of the box.


# References:

* Zamir, Mair, Katelyn Norton, Arlene Fleischhauer, Maria F. Frances, Ruma Goswami, Charlotte W. Usselman, Robert P. Nolan, and J. Kevin Shoemaker. "Dynamic responsiveness of the vascular bed as a regulatory mechanism in vasomotor control." Journal of General Physiology 134, no. 1 (2009): 69-75.

* Zamir, M., R. Goswami, D. Salzer, and J. K. Shoemaker. "Role of vascular bed compliance in vasomotor control in human skeletal muscle." Experimental Physiology 92, no. 5 (2007): 841-848.

* Zamir, Mair. The physics of coronary blood flow. Springer Science & Business Media, 2006.
